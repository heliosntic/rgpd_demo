<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/


class hel_form extends Module
{

    public function __construct()
    {
        $this->name = 'hel_form';
        $this->tab = 'administration';
        $this->version = '1.0';
        $this->author = 'Helios NTIC';
        $this->bootstrap = true;
        $this->need_instance = 0;
        $this->controllers =array("front","frontkit");
        $this->lang = true;
        parent::__construct();

        $this->controllers = array("display","landing");

        $this->displayName = 'Form Generator';
        $this->description = 'Help creating forms';
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->confirmUninstall = 'Etes-vous sûr de vouloir supprimer le module ?';
    }

    public function install()
    {
        include(dirname(__FILE__) . '/sql/install.php');
        Configuration::updateValue('HEL_FORM_CAPTCHA_PUBLIC_KEY','');
        Configuration::updateValue('HEL_FORM_CAPTCHA_SECRET_KEY','');
        return parent::install() &&
            //$this->registerHook('moduleRoutes') &&
            $this->registerHook('hookHeader') &&
            $this->installModuleTab('AdminHelForm', 'Formulaires', '') &&
            $this->installModuleTab('AdminHelFormSubmissions', 'Hel Form Submissions', 'AdminHelForm', false);
    }

    public function uninstall()
    {
        return $this->uninstallModuleTab('AdminHelForm') &&
            $this->uninstallModuleTab('AdminHelFormSubmissions') &&
            parent::uninstall();
    }

    public function installModuleTab($class, $name, $parent_tab, $active = true) {
        $tabParent = intval(Db::getInstance()->getValue('SELECT `id_tab` FROM `' . _DB_PREFIX_ . 'tab` WHERE `class_name` = "' . $parent_tab . '"'));
        $tab = new Tab();

        foreach (Language::getLanguages() as $language)
            $tab->name[$language['id_lang']] = $name;

        $tab->class_name = $class;
        $tab->module = $this->name;
        $tab->id_parent = $tabParent;
        $tab->active = $active;

        if (!$tab->save()) {
            return false;
        }

        return true;
    }

    private function uninstallModuleTab($tab_class)
    {
        $id_tab = Tab::getIdFromClassName($tab_class);

        if ($id_tab != 0) {
            $tab = new Tab($id_tab);
            $tab->delete();
            return true;
        }

        return false;
    }

    public function getContent()
    {
        $output = '';
        if (Tools::isSubmit('submitHelFormConfig'))
        {
            Configuration::updateValue('HEL_FORM_CAPTCHA_PUBLIC_KEY', Tools::getValue('HEL_FORM_CAPTCHA_PUBLIC_KEY'));
            Configuration::updateValue('HEL_FORM_CAPTCHA_SECRET_KEY', Tools::getValue('HEL_FORM_CAPTCHA_SECRET_KEY'));
            $output .= $this->displayConfirmation($this->l('Settings updated'));
        }
        return $output.$this->renderForm();
    }

    public function renderForm(){
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Captcha Public Key'),
                        'name' => 'HEL_FORM_CAPTCHA_PUBLIC_KEY'
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Captcha Secret Key'),
                        'name' => 'HEL_FORM_CAPTCHA_SECRET_KEY'
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save')
                )
            )
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $this->fields_form = array();

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitHelFormConfig';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }

    public function getConfigFieldsValues()
    {
        return array(
            'HEL_FORM_CAPTCHA_PUBLIC_KEY' => Tools::getValue('HEL_FORM_CAPTCHA_PUBLIC_KEY', Configuration::get('HEL_FORM_CAPTCHA_PUBLIC_KEY')),
            'HEL_FORM_CAPTCHA_SECRET_KEY' => Tools::getValue('HEL_FORM_CAPTCHA_SECRET_KEY', Configuration::get('HEL_FORM_CAPTCHA_SECRET_KEY')),
        );
    }

//    public function hookModuleRoutes($params) {
//
//        return array(
//            'module-hel_form-display' => array( //Prestashop will use this pattern to compare addresses: module-{module_name}-{controller_name}
//                'controller' => 'display', //module controller name
//                'rule' =>'hel_form/{idForm}-{rewrite}',
//                'keywords' => array(
//                    'idForm' => array('regexp' => '[0-9]+', 'param' => 'id'),
//                    'rewrite' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
//                ),
//                'params' => array(
//                    'fc' => 'module',
//                    'module' => 'hel_form', //module name
//                    'controller' => 'display'
//
//                )
//            ),
//        );
//    }

    public function hookHeader()
    {
        $this->context->controller->addCSS($this->_path.'/views/css/hel_form.css');
    }

}
