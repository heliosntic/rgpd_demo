<?php

class FrontController extends FrontControllerCore
{

    public function postProcess()
    {
        if (Tools::getValue("has_hel_rgpd_traitement")) {
            require_once(_PS_MODULE_DIR_ . "hel_rgpd/hel_rgpd.php");
            $array_hel_rgpd_consentement_values = array_filter($_REQUEST, function ($key) {
                return strpos($key, 'hel_rgpd_traitement_value_') === 0;
            }, ARRAY_FILTER_USE_KEY);

            foreach ($array_hel_rgpd_consentement_values as $index=>$array_hel_rgpd_consentement_value) {
                Hel_rgpd::addConsentement(explode("|", $array_hel_rgpd_consentement_value, 'form'));
                unset($_POST["hel_rgpd_traitement_value_1"]);
            }
        }
        parent::postProcess();
    }
}
