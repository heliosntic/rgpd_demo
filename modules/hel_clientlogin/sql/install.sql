--
-- Structure de la table `PREFIX_clientlogin_history`
--

CREATE TABLE IF NOT EXISTS `PREFIX_clientlogin_history` (
  `id_clientlogin_history` int(11) NOT NULL AUTO_INCREMENT,
  `id_customer` int(11) NOT NULL,
  `id_employee` int(11) NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_clientlogin_history`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

ALTER TABLE  `PREFIX_orders` ADD  `id_employee` INT NULL;