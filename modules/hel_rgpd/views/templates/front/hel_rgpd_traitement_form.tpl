<div id="hel_rgpd_form">
    {if $hel_rgpd_traitement->hasConsentement == 1}
        <p class="col-sm-2">
            <input type="hidden" name="has_hel_rgpd_traitement" value="1">
            <input type="hidden" name="hel_rgpd_traitement_value_{$hel_rgpd_traitement->id}"
                   id="hel_rgpd_traitement_value_{$hel_rgpd_traitement->id}" value="{$hel_rgpd_traitement->id}|0">
            <label class="switch" onclick="updateSlider(this);">
                <input type="checkbox" name="hel_rgpd_traitement[]" id="hel_rgpd_traitement_{$hel_rgpd_traitement->id}"
                       value="{$hel_rgpd_traitement->id}">

                <span class="slider round"></span>
            </label>
            {str_replace("##finaliteTraitement##",$hel_rgpd_traitement->finaliteTraitement ,$hel_rgpd_traitement->description)}
        </p>
        {if $hel_rgpd_consentement_customer != null}
            <p class="consentement">
                {if $hel_rgpd_consentement_customer->decision == 1}
                    {l s='Vous avez consenti à ce traitement le' mod='hel_rgpd'} {date("d/m/Y",strtotime($hel_rgpd_consentement_customer->date_add))}
                {else}
                    {l s='Vous n\'avez pas consenti à ce traitement le' mod='hel_rgpd'} {date("d/m/Y",strtotime($hel_rgpd_consentement_customer->date_add))}
                {/if}
            </p>
        {/if}
    {/if}
    <p class="lien_detail">
        <a href="{$link->getModuleLink("hel_rgpd","traitementDetail",["hel_rgpd_traitement_id"=>$hel_rgpd_traitement->id])}"
           target="_blank">{l s='Consulter les détails du traitement' mod='hel_rgpd'}</a>
    </p>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('input:checkbox').each(function (index, element) {
            if ($(element).attr('id').indexOf('hel_rgpd_traitement') != -1 && $(element).parent('span').hasClass('checked')) {
                $(element).parent('span').parent('div').addClass('checked');
            } else {
                $(element).parent('span').parent('div').removeClass('checked');
            }
        })
    })

    function updateSlider(element) {
        if ($(element).find('span').hasClass('checked')) {
            $(element).children('div').addClass('checked');
        } else {
            $(element).children('div').removeClass('checked');
        }
    }

    $("#hel_rgpd_traitement_{$hel_rgpd_traitement->id}").parents("form").on("submit", function (e) {
        if ($("#hel_rgpd_traitement_{$hel_rgpd_traitement->id}").is(":checked") == false) {
            $("#hel_rgpd_traitement_value_{$hel_rgpd_traitement->id}").val("{$hel_rgpd_traitement->id}|0");
            {if $hel_rgpd_traitement->obligatoire == 1}
            if (messageSiObligatoire{$hel_rgpd_traitement->id} != null && messageSiObligatoire{$hel_rgpd_traitement->id}!= "") {
                alert(messageSiObligatoire{$hel_rgpd_traitement->id});
            }
            else {
                alert("Vous devez cocher la case {$hel_rgpd_traitement->nameTraitement} pour pouvoir valider le formulaire");
            }
            e.preventDefault();
            return false;
            {/if}
        }
        else {
            $("#hel_rgpd_traitement_value_{$hel_rgpd_traitement->id}").val("{$hel_rgpd_traitement->id}|1");
        }
    });
</script>


