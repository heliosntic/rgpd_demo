<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from CREATYM
 * Use, copy, modification or distribution of this source file without written
 * license agreement from CREATYM is strictly forbidden.
 * In order to obtain a license, please contact us: info@creatym.fr
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe CREATYM
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de CREATYM est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter CREATYM a l'adresse: info@creatym.fr
 * ...........................................................................
 *
 * @package   Client login
 * @author    Benjamin L.
 * @copyright 2015 Créatym <http://modules.creatym.fr>
 * @license   Commercial license
 * Support by mail  :  info@creatym.fr
 * Support on forum :  clientlogin
 * Phone : +33.87230110
 */

require_once(dirname(__FILE__) . '/../../classes/ClientLoginHistory.php');

class AdminClientLoginController extends ModuleAdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'clientlogin_history';
        $this->className = 'ClientLoginHistory';
        $this->list_id = 'clientlogin_history';
        $this->identifier = 'id_clientlogin_history';
        $this->_orderBy = 'date_add';
        $this->_orderWay = 'DESC';

        $this->_select = '
		c.firstname AS firstname,
		c.lastname AS lastname,
		c.email AS email,
		CONCAT(LEFT(e.`firstname`, 1), \'. \', e.`lastname`) AS `employee`';

        $this->_join = '
		LEFT JOIN `' . _DB_PREFIX_ . 'customer` c ON (c.`id_customer` = a.`id_customer`)
		LEFT JOIN `' . _DB_PREFIX_ . 'employee` e ON (e.`id_employee` = a.`id_employee`)';

        $this->fields_list = array(
            'id_clientlogin_history' => array(
                'title' => $this->l('ID'),
                'align' => 'center',
                'class' => 'fixed-width-xs'
            ),
            'firstname' => array(
                'title' => $this->l('First name')
            ),
            'lastname' => array(
                'title' => $this->l('Last name')
            ),
            'email' => array(
                'title' => $this->l('Email address')
            ),
            'employee' => array(
                'title' => $this->l('Employee')
            ),
            'date_add' => array(
                'title' => $this->l('Date'),
                'align' => 'text-right',
                'type' => 'datetime',
                'filter_key' => 'a!date_add'
            ),
        );

        $this->list_no_link = true;
        $this->_use_found_rows = false;

        parent::__construct();
        if (!$this->module->active)
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminHome'));
    }

    public function setMedia()
    {
        if (version_compare(_PS_VERSION_, '1.6.0', '>=') === true)
            $this->addJs(_MODULE_DIR_ . $this->module->name . '/views/js/clientlogin_bt.js');
        else
            $this->addJs(_MODULE_DIR_ . $this->module->name . '/views/js/clientlogin.js');

        return parent::setMedia();
    }

    public function initToolBarTitle()
    {
        $this->toolbar_title[] = $this->l('Log');
    }

    public function initPageHeaderToolbar()
    {
        parent::initPageHeaderToolbar();
        unset($this->page_header_toolbar_btn['back']);
    }

    public function initToolbar()
    {
        parent::initToolbar();
        $this->toolbar_btn['delete'] = array(
            'short' => 'Erase',
            'desc' => $this->l('Erase all'),
            'js' => 'if (confirm(\'' . $this->l('Are you sure?') . '\')) document.location = \'' . Tools::safeOutput($this->context->link->getAdminLink('AdminClientLogin')) . '&amp;token=' . $this->token . '&amp;deletelog=1\';'
        );
        unset($this->toolbar_btn['new']);
    }

    public function postProcess()
    {
        if (Tools::getValue('deletelog')) {
            if (ClientLoginHistory::eraseAllLogs()) {
                Tools::redirectAdmin(Context::getContext()->link->getAdminLink('AdminClientLogin'));
            }
        }

        return parent::postProcess();
    }

    public function ajaxProcessClientLoginTasks()
    {
        $return = array(
            'header_notification' => $this->module->renderHeaderNotification(),
        );

        die(Tools::jsonEncode($return));
    }

    public function ajaxProcessClientLoginDirectLink()
    {
        $cookie_lifetime = (int)defined('_PS_ADMIN_DIR_') ? Configuration::get('PS_COOKIE_LIFETIME_BO') : Configuration::get('PS_COOKIE_LIFETIME_FO');
        $cookie_lifetime = time() + (max($cookie_lifetime, 1) * 3600);

        if (Context::getContext()->shop->getGroup()->share_order)
            $cookie = new Cookie('ps-sg' . Context::getContext()->shop->getGroup()->id, '', $cookie_lifetime, Context::getContext()->shop->getUrlsSharedCart());
        else {
            $domains = null;

            if (Context::getContext()->shop->domain != Context::getContext()->shop->domain_ssl)
                $domains = array(Context::getContext()->shop->domain_ssl, Context::getContext()->shop->domain);

            $cookie = new Cookie('ps-s' . Context::getContext()->shop->id, '', $cookie_lifetime, $domains);
        }

        if ($cookie->logged)
            $cookie->logout();

        Tools::setCookieLanguage();
        Tools::switchLanguage();

        $customer = new Customer((int)Tools::getValue('id_customer'));
        $cookie->id_customer = (int)$customer->id;
        $cookie->customer_lastname = $customer->lastname;
        $cookie->customer_firstname = $customer->firstname;
        $cookie->logged = 1;
        $cookie->passwd = $customer->passwd;
        $cookie->email = $customer->email;
        if (Module::isInstalled("hel_b2b") && Module::isEnabled("hel_b2b") &&  Configuration::get('HEL_B2B_MULTI_USERS', false)) {
            $cookie->id_hel_b2b_users = HelB2bUser::getFirstByCustomer($customer->id);
        }

        if (Configuration::get('PS_CART_FOLLOWING') && (empty($cookie->id_cart) || Cart::getNbProducts($cookie->id_cart) == 0))
            $cookie->id_cart = Cart::lastNoneOrderedCart($customer->id);

        $cookie->id_cart = $this->module->getLastCart($customer->id);

        $clientLoginHistory = new ClientLoginHistory();
        $clientLoginHistory->id_customer = (int)$customer->id;
        $clientLoginHistory->id_employee = (int)Context::getContext()->employee->id;
        $clientLoginHistory->date_add = date('Y-m-d H:i:s');
        $clientLoginHistory->add();

        Tools::redirect(Context::getContext()->link->getPageLink('my-account'));
    }

    public function processClientLoginDirectLink()
    {
        return $this->ajaxProcessClientLoginDirectLink();
    }
}