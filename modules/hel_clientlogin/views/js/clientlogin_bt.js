/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from CREATYM
 * Use, copy, modification or distribution of this source file without written
 * license agreement from CREATYM is strictly forbidden.
 * In order to obtain a license, please contact us: info@creatym.fr
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe CREATYM
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de CREATYM est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter CREATYM a l'adresse: info@creatym.fr
 * ...........................................................................
 *
 * @package   Client login
 * @author    Benjamin L.
 * @copyright 2015 Créatym <http://modules.creatym.fr>
 * @license   Commercial license
 * Support by mail  :  info@creatym.fr
 * Support on forum :  clientlogin
 * Phone : +33.87230110
 */

$(document).ready( function () {
	clientLoginTasks();

	$(document).on("keyup", "#clientlogin_search", function(){
		searchClientLoginAsCustomers(); 
	});
});

function clientLoginTasks()
{
	$('#clientlogin_notif').remove();
	$('#header_notifs_icon_wrapper').append('<div id="clientlogin_notif" class="notifs"></div>');
	$.ajax({
		type: 'POST',
		url: admin_clientlogin_ajax_url,
		dataType: 'json',
		data: {
			controller : 'AdminClientLogin',
			action : 'clientLoginTasks',
			ajax : true,
			client_login_customer : client_login_customer,
			client_login_order : client_login_order,
		},
		success: function(jsonData)
		{
			initClientLoginHeaderNotification(jsonData.header_notification);
		}
	});
}

function initClientLoginHeaderNotification(html)
{
	$('#clientlogin_notif').remove();
	$('#header_notifs_icon_wrapper').append(html);
	
	if ($('.dropdown-toggle').length)
		$('.dropdown-toggle').dropdown();
}

function searchClientLoginAsCustomers() {
	var _customer_search = $('#clientlogin_search').val();

	$.ajax({
		type: 'POST',
		headers: { "cache-control": "no-cache" },
		url: 'ajax-tab.php' + '?rand=' + new Date().getTime(),
		async: true,
		dataType: 'json',
		data: {
			controller: 'AdminCustomers',
			token: admincustomer_token,
			action: 'searchCustomers',
			ajax: 1,
			customer_search: _customer_search
		},
		success : function(res)
		{
			var customers_found = '<div class="panel clearfix" style="margin-bottom:0px; box-shadow:none;">';
			
			if (res.found)
			{
				_customer_searches = _customer_search.split(' ');
				var _class_line = '';
				var element_index = 0;
				
				$.each(res.customers, function(index) {
					var _clientlogin_url = admin_clientlogin_ajax_url + '&action=clientLoginDirectLink&id_customer=' + this.id_customer;
					var clientlogin_new_tab = " target=_blank";
					
					if (element_index < 1) { _class_line = ''; }
					else { _class_line = ' clientlogin_line'; }
					customers_found += '<div class="panel-heading' + _class_line + '">\
											<i class="icon-user"></i> ' + this.firstname + ' ' + this.lastname + ' [ ' + this.id_customer + ' ]&nbsp;-&nbsp;\
											<i class="icon-envelope"></i> <a class="dropdown_clientlogin_link" href="' + _clientlogin_url + '"' + clientlogin_new_tab + '>' + this.email + '</a>\
											<div class="panel-heading-action" style="float:right;">\
												<a class="dropdown_clientlogin_link" href="' + _clientlogin_url + '"' + clientlogin_new_tab + '><i class="icon-chevron-circle-right"></i></a>\
											</div>\
										</div>';
										
					element_index++;
				});

				$('#clientlogin_search_result').html(customers_found + '</div>');
			}
			else {
				$('#clientlogin_search_result').html('<span class="no_results"> ' + clientlogin_no_results_txt + ' </span>');
			}
		}
	});
}