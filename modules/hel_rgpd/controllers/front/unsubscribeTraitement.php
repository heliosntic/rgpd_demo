<?php

require_once(dirname(__FILE__) . '/../../classes/HelTraitement.php');
require_once(dirname(__FILE__) . '/../../classes/HelConsentementCustomer.php');

class hel_rgpdunsubscribeTraitementModuleFrontController extends ModuleFrontController
{
    public $auth = false;
    public $ssl = true;

    public function __construct()
    {
        parent::__construct();
        $this->display_column_left = false;
        $this->context = Context::getContext();
    }

    public function setMedia()
    {
        parent::setMedia();
        $link = new Link();
        Media::addJsDef(array("hef_rgpd_link_consentement_change"=>$link->getModuleLink($this->module->name,"changeConsentement")));
        $this->addJS(_MODULE_DIR_ . $this->module->name . '/views/js/hel_rgpd_list_consentement_customer.js');
    }
    public function initContent()
    {

        parent::initContent();
        $id_customer = Tools::getValue("id_customer", null);
        $email_customer = Tools::getValue("email_customer");
        $codeTraitement = Tools::getValue("codeTraitement");
        $cookie_key = Tools::getValue("cookie_key");
        $hel_traitement = HelTraitement::getByCode($codeTraitement);
        if($hel_traitement->id == null){
            die(Tools::displayError("Code traitement inconnu"));
        }
        if($email_customer == null && $id_customer == null ){
            die(Tools::displayError("Client introuvable"));
        }
        if($id_customer != ""){
            $customer = new Customer($id_customer);
            if($customer->id == null){
                die(Tools::displayError("Client inexistant"));
            }
        }
        if($cookie_key == null || $cookie_key != md5($hel_traitement->cookie_key.$hel_traitement->codeTraitement)){
            die(Tools::displayError("Token incorrect"));
        }

        $traitement_last_decision = false;
        $traitement_last_decision_date = null;
        $hel_rgpd_consentements_customer_array = HelConsentementCustomer::getConsentementsCustomer($id_customer, $hel_traitement->id, $email_customer);
        $consentements_customer = array();
        foreach ($hel_rgpd_consentements_customer_array as $hel_rgpd_consentement_customer_array) {
            $consentements_customer[] = $hel_rgpd_consentement_customer_array;
            if ($hel_rgpd_consentement_customer_array["active"] == 1) {
                $traitement_last_decision = $hel_rgpd_consentement_customer_array["decision"];
                $traitement_last_decision_date = $hel_rgpd_consentement_customer_array["date_add"];
            }
        }

        $this->context->smarty->assign(array(
            'hel_traitement' => $hel_traitement,
            'id_customer' => $id_customer,
            'email_customer' => $email_customer,
            'traitement_last_decision' => $traitement_last_decision,
            'traitement_last_decision_date' => $traitement_last_decision_date,
            'consentements_customer' => $consentements_customer,
        ));
        $this->setTemplate('hel_rgpd_unsubscribe_traitement.tpl');
    }
}