<?php

/**
 * Created by PhpStorm.
 * User: b.bonnin
 * Date: 28/03/2018
 * Time: 16:58
 */
class HelConsentementCustomer extends ObjectModel
{
    public $id_hel_rgpd_consentement_customer;
    public $id_shop;
    public $id_hel_rgpd_traitement;
    public $id_customer;
    public $identifiant;
    public $decision;
    public $active;
    public $date_add;
    public $date_validity;
    public $origin;
    public $origin_referrer;

    public static $definition = array(
        'table' => 'hel_rgpd_consentement_customer',
        'primary' => 'id_hel_rgpd_consentement_customer',
        'multilang' => false,
        'fields' => array(
            'id_hel_rgpd_traitement' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
            'id_shop' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
            'id_customer' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => false),
            'identifiant' => array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'lang' => false, 'required' => false, 'size' => 1024),
            'decision' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
            'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'required' => true),
            'active' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
            'date_validity' => array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'required' => false),
            'origin' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'lang' => false, 'required' => false, 'size' => 10),
            'origin_referrer' => array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'lang' => false, 'required' => false, 'size' => 1024),

        ),
    );
    public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
        if($id_shop!==null){
            $this->id_shop=$id_shop;
        }
        return parent::__construct($id,$id_lang,$id_shop);
    }
    protected $webserviceParameters = array(
        'objectsNodeName' => 'hel_rpgd_consentement_customer',
        'fields' => array(
            'id_customer' => array('xlink_resource'=> 'customers')
        ),
    );

    public function getWebserviceParameters($ws_params_attribute_name = null)
    {
        $resource_parameters = parent::getWebserviceParameters($ws_params_attribute_name);
        foreach ($resource_parameters["fields"] as $key=>$field){
            if($key != "decision" && $key != "date_add"){
                unset($resource_parameters["fields"][$key]);
            }
        }
        return $resource_parameters;
//        krumo($resource_parameters);
//        exit();
    }
    public function getWebserviceObjectList($sql_join, $sql_filter, $sql_sort, $sql_limit)
    {
        return false;
    }


    public static function getConsentementsCustomer($id_customer, $id_hel_rgpd_traitement = null, $email_customer = null)
    {
        $db = Db::getInstance();
        $sql = "SELECT * FROM "._DB_PREFIX_."hel_rgpd_consentement_customer WHERE 1=1";

        if($id_customer != null){
            $sql .= " AND id_customer=$id_customer ";
        }
        if($email_customer != null){
            $sql .= " AND identifiant='$email_customer'";
        }
        if($id_hel_rgpd_traitement != null){
            $sql .= " AND id_hel_rgpd_traitement=$id_hel_rgpd_traitement";
        }
        $sql .=" ORDER BY date_add DESC";
//        krumo($sql);
        $res = $db->executeS($sql);
        return $res;
    }

    public static function getLastConsentementsCustomer($id_customer, $identifiant, $id_traitement, $date_consentement=null)
    {
        $sql = "SELECT id_hel_rgpd_consentement_customer FROM "._DB_PREFIX_."hel_rgpd_consentement_customer WHERE id_hel_rgpd_traitement=$id_traitement";
        if($id_customer!= "" && $id_customer!=null){
            $sql .= " AND id_customer=".$id_customer;
        }
        else{
            $sql .= " AND identifiant='".$identifiant."'";
        }
        if($date_consentement == null){
            $sql .= " AND active=1";
        }
        else{
            $sql .= " AND date_add <= '".$date_consentement."' ORDER BY date_add DESC";
        }
//        $sql .= " LIMIT 0,1";
//        krumo($sql);
//        exit();
        return new HelConsentementCustomer(Db::getInstance()->getValue($sql));

    }

    public static function unactiveConsentementCustomer($id_hel_rgpd_traitement,$id_customer,$identifiant){
        $db = Db::getInstance();

        if($id_customer == null || $id_customer == "" || $id_customer == 0){
            $sql = "UPDATE "._DB_PREFIX_."hel_rgpd_consentement_customer SET active=0 WHERE identifiant='".$identifiant."' AND id_hel_rgpd_traitement=$id_hel_rgpd_traitement ";
        }
        else{
            $sql = "UPDATE "._DB_PREFIX_."hel_rgpd_consentement_customer SET active=0 WHERE id_customer=$id_customer AND id_hel_rgpd_traitement=$id_hel_rgpd_traitement ";
        }
//        krumo($sql);
        return $db->execute($sql);
    }
}