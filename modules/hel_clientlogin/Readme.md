# Client login Module for Prestashop

## About

Client login module will allow you to connect to customers accounts whithout get their IDs.

Features :

## Install

Easy to install, everything is automated
Simply upload the module into the modules folder of your website
Then in the backoffice, click on "install"

To enable this module for Prestashop :
1. Log into prestashop
2. Click on the 'Modules' tab
3. Click 'Install'
