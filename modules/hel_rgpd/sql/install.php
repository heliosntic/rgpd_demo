<?php
/**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2018 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

$sql = array();

$sql[] = "CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."hel_rgpd_traitement` (
    `id_hel_rgpd_traitement` int(11) NOT NULL AUTO_INCREMENT,
    `nameTraitement` varchar(255) NOT NULL,
    `decisionAutomatisee` tinyint(1) DEFAULT '0',
    `active` tinyint(1) DEFAULT '1',
    `obligatoire` tinyint(1) DEFAULT '1',
    `afficheStateInForm` tinyint(1) DEFAULT '0',
    `duree` int(11) NOT NULL,
    `champ_identifiant` varchar(255) NULL,
    `codeTraitement` varchar(20) NOT NULL,
    `PFGidForms` varchar(255) NULL,
    `hasConsentement` tinyint(1) DEFAULT '1',
    `cookie_key` varchar(255) NULL,
     PRIMARY KEY (`id_hel_rgpd_traitement`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";

$sql[] = "CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."hel_rgpd_traitement_shop` (
    `id_hel_rgpd_traitement` int(11) NOT NULL,
    `id_shop` int(11) NOT NULL,
     PRIMARY KEY (`id_hel_rgpd_traitement`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

$sql[] = "CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."hel_rgpd_traitement_lang` (
    `id_hel_rgpd_traitement` int(11) NOT NULL,
    `id_lang` int(11) NOT NULL,
    `description` varchar(1024) NOT NULL,
    `finaliteTraitement` varchar(1024) NOT NULL,
    `typesDonnees` varchar(1024) NOT NULL,
    `transfertsDonnees` varchar(1024) NOT NULL,
    `procedureRetrait` varchar(1024) NOT NULL,
    `responsableTraitement` varchar(1024) NOT NULL,
    `messageSiObligatoire` varchar(1024) NULL,
     PRIMARY KEY (`id_hel_rgpd_traitement`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

$sql[] = "CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."hel_rgpd_consentement_customer` (
    `id_hel_rgpd_consentement_customer` int(11) NOT NULL AUTO_INCREMENT,
    `id_hel_rgpd_traitement` int(11) NOT NULL,
    `id_shop` int(11) NOT NULL,
    `id_customer`  int(11) NULL,
    `identifiant`  varchar(1024) NULL,
    `decision` tinyint(1) DEFAULT '0',
    `active` tinyint(1) DEFAULT '1',
    `date_add` Datetime NOT NULL,
    `date_validity` Datetime NULL,
    `origin` VARCHAR(10) NULL,
    `origin_referrer` VARCHAR(1024) NULL,
     PRIMARY KEY (`id_hel_rgpd_consentement_customer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";



foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
