$(document).ready(function () {
    $(".change_consentement").on("click", function () {
        data = "customer_id=" + $(this).data("customer") + "&hel_rgpd_traitement_id=" + $(this).data("traitement")+"&identifiant="+$(this).data("identifiant");
        // data = "customer_id=" + $(this).data("customer") + "&hel_rgpd_traitement_id=" + $(this).data("traitement") + "&decision=" + $(this).data("decision");
        $.ajax(
            {
                type: 'GET',
                url: hef_rgpd_link_consentement_change,
                data: data,
                dataType: 'json',
                success: function (result) {
                    $("#hel_rgpd_traitement"+result.hel_consentement_customer.id_hel_rgpd_traitement).children('span.change_consent').children('a').children('label').children('div').toggleClass('checked');
                    $("#hel_rgpd_traitement"+result.hel_consentement_customer.id_hel_rgpd_traitement).find('i').toggleClass('icon-check-circle');
                    $("#hel_rgpd_traitement"+result.hel_consentement_customer.id_hel_rgpd_traitement).find('i').toggleClass('icon-times-circle');
                    $("#hel_rgpd_traitement"+result.hel_consentement_customer.id_hel_rgpd_traitement).find('i').toggleClass('green');
                    $("#hel_rgpd_traitement"+result.hel_consentement_customer.id_hel_rgpd_traitement).find('i').toggleClass('red');


                    if(result.hel_consentement_customer.decision == true){
                        decision_html ="<i class='icon-check-circle green'></i>";
                    }
                    else{
                        decision_html ="<i class='icon-times-circle red'></i>";
                    }

                    var date = new Date(result.hel_consentement_customer.date_add);
                    var mm = date.getMonth() + 1;
                    var MM = date.getMinutes();
                    if(mm < 10){
                        mm = "0" + mm;
                    }
                    if(MM < 10){
                        MM = "0" + MM;
                    }
                    date = date.getDate() + '/' + mm + '/' + date.getFullYear() + ' ' + date.getHours() + ':' + MM;

                    $("#last_upd"+result.hel_consentement_customer.id_hel_rgpd_traitement).html(date);
                    $("#traitement_last_decision"+result.hel_consentement_customer.id_hel_rgpd_traitement).html(decision_html);
                    $("#change_consentement"+result.hel_consentement_customer.id_hel_rgpd_traitement).data("decision",decision_html);
                    $("#hel_rgpd_consentement_customer_list"+result.hel_consentement_customer.id_hel_rgpd_traitement + " tbody").prepend('<tr class="hel_rgpd_consentement_customer_detail" ><td>'+date+'</td><td>'+decision_html+'</td></tr>');
                }
            });
    });
});