<?php


require_once(dirname(__FILE__) . '/../../classes/HelTraitement.php');
require_once(dirname(__FILE__) . '/../../classes/HelConsentementCustomer.php');

class hel_rgpdlistConsentementCustomerModuleFrontController extends ModuleFrontController
{
    public $auth = false;
    public $ssl = true;

    public function __construct()
    {
        parent::__construct();
        $this->display_column_left = false;
        $this->context = Context::getContext();
    }
    public function setMedia()
    {
        parent::setMedia();
        $link = new Link();
        Media::addJsDef(array("hef_rgpd_link_consentement_change"=>$link->getModuleLink($this->module->name,"changeConsentement")));
        $this->addJS(_MODULE_DIR_ . $this->module->name . '/views/js/hel_rgpd_list_consentement_customer.js');
    }

    public function initContent()
    {

        parent::initContent();
//        $hel_rgpd_consentement_id = Tools::getValue("hel_rgpd_consentement_id");
        if(isset($this->context->customer->id) && $this->context->customer->id) {
            $hel_rgpd_consentements_customer_array = HelConsentementCustomer::getConsentementsCustomer($this->context->customer->id);
            $hel_rgpd_consentements_customer = array();
            foreach ($hel_rgpd_consentements_customer_array as $hel_rgpd_consentement_customer_array) {
                $hel_rgpd_consentements_customer[$hel_rgpd_consentement_customer_array["id_hel_rgpd_traitement"]]["consentements_customer"][] = $hel_rgpd_consentement_customer_array;
                if (!isset($hel_rgpd_consentements_customer[$hel_rgpd_consentement_customer_array["id_hel_rgpd_traitement"]]["traitement"])) {
                    $hel_rgpd_consentements_customer[$hel_rgpd_consentement_customer_array["id_hel_rgpd_traitement"]]["traitement"] = new HelTraitement($hel_rgpd_consentement_customer_array["id_hel_rgpd_traitement"]);
                }
                if ($hel_rgpd_consentement_customer_array["active"] == 1) {
                    $hel_rgpd_consentements_customer[$hel_rgpd_consentement_customer_array["id_hel_rgpd_traitement"]]["traitement_last_decision"] = $hel_rgpd_consentement_customer_array["decision"];
                    $hel_rgpd_consentements_customer[$hel_rgpd_consentement_customer_array["id_hel_rgpd_traitement"]]["traitement_last_decision_date"] = $hel_rgpd_consentement_customer_array["date_add"];
                }
            }

            $this->context->smarty->assign(array(
                'hel_rgpd_consentements_customer' => $hel_rgpd_consentements_customer,
                'id_customer' => $this->context->customer->id,
            ));

        }else{
            $this->context->smarty->assign(array(
                'hel_rgpd_consentements_customer' => array(),
                'id_customer' => 0,
            ));
        }

        $this->setTemplate('hel_rgpd_list_consentement_customer.tpl');
    }
}