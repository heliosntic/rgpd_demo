{*
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from CREATYM
* Use, copy, modification or distribution of this source file without written
* license agreement from CREATYM is strictly forbidden.
* In order to obtain a license, please contact us: info@creatym.fr
* ...........................................................................
* INFORMATION SUR LA LICENCE D'UTILISATION
*
* L'utilisation de ce fichier source est soumise a une licence commerciale
* concedee par la societe CREATYM
* Toute utilisation, reproduction, modification ou distribution du present
* fichier source sans contrat de licence ecrit de la part de CREATYM est
* expressement interdite.
* Pour obtenir une licence, veuillez contacter CREATYM a l'adresse: info@creatym.fr
* ...........................................................................
*
* @package   Client login
* @author    Benjamin L.
* @copyright 2015 Créatym <http://modules.creatym.fr>
* @license   Commercial license
* Support by mail  :  info@creatym.fr
* Support on forum :  clientlogin
* Phone : +33.87230110
*}

{if $smarty.const._PS_VERSION_ >= 1.6}
	{if $page_name == "order"}<div class="row">{/if}

	<div class="col-lg-{if $page_name == "order"}7{else}6{/if}">
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-user"></i>
				{l s='Client login' mod='hel_clientlogin'}
				
				{if isset($customer_logged) && $customer_logged}
					<div class="panel-heading-action">
						<a class="btn btn-default" href="{$link->getPageLink('index.php', true, NULL, "mylogout")|escape:'html':'UTF-8'}">
							<i class="icon-power-off"></i>
							{l s='Log out' mod='hel_clientlogin'}
						</a>
					</div>
				{/if}
			</div>
		
			<div class="form-horizontal">
				{if isset($customer_logged) && $customer_logged}
					<p class="text-muted text-center">{l s='You are connected as' mod='hel_clientlogin'} {$customer->firstname|escape:'html':'UTF-8'} {$customer->lastname|escape:'html':'UTF-8'}.</p>
					<div class="hidden-print">
						<center>
							<a class="btn btn-default" href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" target="_blank">
								<i class="icon-user"></i>
								{l s='Go to customer account' mod='hel_clientlogin'}
							</a>
							<a class="btn btn-default" href="{$link->getPageLink('order', true)|escape:'html':'UTF-8'}" target="_blank">
								<i class="icon-shopping-cart"></i>
								{l s='Go to customer cart' mod='hel_clientlogin'}
							</a>
						</center>
					</div>
				{else}
					<form method="post" action="" class="well">
						<input type="hidden" name="id_customer" value="{$customer->id|escape:'html':'UTF-8'}" />
						
						<div class="row">
							<div class="col-lg-9">
								<label class="control-label col-lg-6">{l s='Load last cart' mod='hel_clientlogin'}</label>
								<div class="col-lg-6">
									<span class="switch prestashop-switch fixed-width-lg">
										<input type="radio" name="used_last_cart" id="used_last_cart_on" value="1" checked="checked" />
										<label for="used_last_cart_on">
											{l s='Yes' mod='hel_clientlogin'}
										</label>
										<input type="radio" name="used_last_cart" id="used_last_cart_off" value="0" />
										<label for="used_last_cart_off">
											{l s='No' mod='hel_clientlogin'}
										</label>
										<a class="slide-button btn"></a>
									</span>
								</div>
							</div>
							<div class="col-lg-3">
								<button id="submitClientLogin" name="submitClientLogin" class="btn btn-primary" type="submit">
									{l s='Log to customer' mod='hel_clientlogin'}
								</button>
							</div>
						</div>
					</form>
				{/if}
			</div>
		</div>
	</div>

	{if $page_name == "order"}</div>{/if}
{else}
	{if $page_name == "order"}
		<br>
		<fieldset>
			<legend>
				<img src="../img/admin/tab-customers.gif">
				{l s='Client login' mod='hel_clientlogin'}
			</legend>
	{else}
		<div class="clear"> </div>
		<div class="separation"></div>
		
		<div style="width:50%;float:left;">
			<h2>{l s='Client login' mod='hel_clientlogin'}</h2>
	{/if}
		{if isset($customer_logged) && $customer_logged}
			<p class="text-muted text-center">{l s='You are connected as' mod='hel_clientlogin'} {$customer->firstname|escape:'html':'UTF-8'} {$customer->lastname|escape:'html':'UTF-8'}.</p>
			<div class="hidden-print">
				<center>
					<div>
						<a class="button" href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" target="_blank">
							<img src="../img/admin/tab-customers.gif">
							{l s='Go to customer account' mod='hel_clientlogin'}
						</a>&nbsp;
						<a class="button" href="{$link->getPageLink('order', true)|escape:'html':'UTF-8'}" target="_blank">
							<img src="../img/admin/cart.gif">
							{l s='Go to customer cart' mod='hel_clientlogin'}
						</a>
					</div>
					<br>
					<div>
						<a class="button" href="{$link->getPageLink('index.php', true, NULL, "mylogout")|escape:'html':'UTF-8'}">
							<img src="../img/admin/nav-logout.gif">
							{l s='Log out' mod='hel_clientlogin'}
						</a>
					</div>
				</center>
			</div>
		{else}
			<form method="post" action="">
				<input type="hidden" name="id_customer" value="{$customer->id|escape:'html':'UTF-8'}" />
				
				<table class="" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="right">
							<b>{l s='Load last cart' mod='hel_clientlogin'}</b>
						</td>
						<td class="center">
							<input id="used_last_cart_off_on" type="radio" checked="checked" value="1" name="used_last_cart_off">
							<label class="t" for="used_last_cart_off_on">
								<img title="{l s='Yes' mod='hel_clientlogin'}" alt="{l s='Yes' mod='hel_clientlogin'}" src="../img/admin/enabled.gif">
							</label>
							<input id="used_last_cart_off_off" type="radio" value="0" name="used_last_cart_off">
							<label class="t" for="used_last_cart_off_off">
								<img title="{l s='No' mod='hel_clientlogin'}" alt="{l s='No' mod='hel_clientlogin'}" src="../img/admin/disabled.gif">
							</label>
						</td>
						<td>
							<button id="submitClientLogin" name="submitClientLogin" class="button" type="submit">
								{l s='Log to customer' mod='hel_clientlogin'}
							</button>
						</td>
					</tr>
				</table>
			</form>
		{/if}
	{if $page_name == "order"}
		</fieldset>
	{else}
		</div>
		<div class="clear"> </div>
		<div class="separation"></div>
	{/if}
{/if}