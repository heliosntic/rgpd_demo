<?php

require_once(dirname(__FILE__) . '/../../classes/HelFormClass.php');
require_once(dirname(__FILE__) . '/../../classes/HelFormSubmissionsClass.php');

class AdminHelFormSubmissionsController extends ModuleAdminController {

    private $titleForm;

    public function __construct() {

        $this->addRowAction('view');
        $this->addRowAction('delete');
        $this->bulk_actions = array('delete' => array('text' => 'Supprimer la selection', 'confirm' => 'Confirmer la suppression ?'));

        if (!Tools::isSubmit('id_hel_form')) {
            $link = new Link();
            Tools::redirectAdmin($link->getAdminLink('AdminHelForm'));
        }

        $db = Db::getInstance();
        $sql = 'SELECT `tableForm`, `jsonFieldsForm`,`nameForm`
        FROM `' . _DB_PREFIX_ . 'hel_form`
        WHERE `id_hel_form` = ' . Tools::getValue('id_hel_form');
        $res = $db->executeS($sql);

        if (empty($res)) {
            $link = new Link();
            Tools::redirectAdmin($link->getAdminLink('AdminHelForm'));
        }

        $tableName = $res[0]['tableForm'];
        $this->bootstrap = true;
        $this->table = $tableName;
        $this->className = 'HelFormSubmissionsClass';
        $this->lang = false;
        $this->titleForm = $res[0]['nameForm'];

        HelFormSubmissionsClass::$definition = array(
            'table' => $tableName,
            'fields' => array()
        );

        $jsonFields = json_decode($res[0]['jsonFieldsForm']);
        $jsonFieldsDisplayBO = array();
        foreach ($jsonFields as $field){
            if($field->displayListBO){
                array_push($jsonFieldsDisplayBO,$field);
                HelFormSubmissionsClass::$definition['fields'][$field->column]=array();
                if (strpos($field->column,'id') !== false){
                    HelFormSubmissionsClass::$definition['primary']=$field->column;
                    $this->identifier = $field->column;
                }
            }
        }
        $this->fields = $jsonFields;

        foreach ($jsonFieldsDisplayBO as $field){
            $this->fields_list[$field->column] = array (
                'title' => empty($field->name) ?  $field->column : $field->name,
                'align' => 'center',
                'width' => 'auto',
                'search' => true,
            );
        }

        parent :: __construct();
        $this->tabAccess = Profile::getProfileAccess($this->context->employee->id_profile, Tab::getIdFromClassName('AdminHelForm'));
    }

    public function renderList()
    {
        $this->initToolbar();
        return parent::renderList();
    }

    public function initBreadcrumbs($tab_id = null, $tabs = null)
    {
        parent::initBreadcrumbs();
        $this->breadcrumbs = array();
        $this->breadcrumbs[] = 'Hel Form';
        $this->breadcrumbs[] = 'Réponses';
        $this->breadcrumbs[] = $this->titleForm;
    }

    public function initProcess()
    {
        parent::initProcess();
        self::$currentIndex .= '&id_hel_form='.(int)Tools::getValue('id_hel_form');
    }
    /**
     * Initialize the toolbar
     * Used by the parent class
     *
     * @see AdminController::initToolbar
     */
    public function initToolbar()
    {
        parent::initToolbar();
        unset($this->toolbar_btn['new']);

        if ($this->display === 'view') {
            $back_url = $this->context->link->getAdminLink('AdminHelFormSubmissions').'&id_hel_form='.Tools::getValue('id_hel_form');
            $this->toolbar_btn['back'] = array(
                'short' => 'Back',
                'href' => $back_url,
                'desc' => 'Retour',
            );
        } else {
            $back_url = $this->context->link->getAdminLink('AdminHelForm');
            $this->toolbar_btn['back'] = array(
                'short' => 'Back',
                'href' => $back_url,
                'desc' => 'Retour aux formulaires',
            );
            $this->toolbar_btn['preview'] = array(
                'href' => $this->context->link->getModuleLink('hel_form','display',array('id'=>Tools::getValue("id_hel_form"))),
                'desc' => 'Voir le formulaire',
                'class' => 'toolbar-new',
            );
        }
        return $this->toolbar_btn;
    }

    public function renderView()
    {
        if(!Tools::isSubmit($this->identifier))
        {
            $link = new Link();
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminHelFormSubmissions').'&id_hel_form='.(int)Tools::getValue('id_hel_form'));
        }

        $db = Db::getInstance();
        $sql = 'SELECT *
        FROM `' . _DB_PREFIX_ . $this->table.'`
        WHERE `'.$this->identifier.'` = ' . Tools::getValue($this->identifier);
        $res = $db->executeS($sql);

        foreach ($this->fields as $field) {
            if (strpos($field->column,'date_soumission') !== false){
                $dateSoumission = $res[0][$field->column];
            }
        }

        $view = parent::renderView();
        $view .= '<div style="background-color: #fff; padding: 20px; border-radius: 3px">';

        if (empty($dateSoumission)){
            $view .= '<h1>Réponses</h1><hr />';
        }else{
            $view .= '<h1>'.sprintf('Réponse du %s',$dateSoumission).'</h1><hr />';
        }

        $view .= '<ul style="margin-top: 40px; list-style-type: none">';
        foreach ($this->fields as $field) {
            $view .='<li style="margin: 15px 0">';
            $view .= '<span style="font-size: 14px;">';
            if (!empty($field->name)){
                $view .='<strong>'.$field->name.'</strong> : '.$res[0][$field->column];
            }else{
                $view .='<strong>'.$field->column.'</strong> : '.$res[0][$field->column];
            }
            $view.='</li>';
        }
        $view .= '</ul>'.'<p style="margin-top: 20px; padding-left: 40px;">';
        $view .= '<a href="'.Context::getContext()->link->getAdminLink('AdminHelFormSubmissions').'&id_hel_form='.(int)Tools::getValue('id_hel_form').'" class="btn btn-default">'.'Retourner à la page des réponses'.'</a></p>';
        $view .= '</div>';

        return $view;
    }
}