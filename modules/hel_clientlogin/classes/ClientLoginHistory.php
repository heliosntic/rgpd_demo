<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from CREATYM
 * Use, copy, modification or distribution of this source file without written
 * license agreement from CREATYM is strictly forbidden.
 * In order to obtain a license, please contact us: info@creatym.fr
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe CREATYM
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de CREATYM est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter CREATYM a l'adresse: info@creatym.fr
 * ...........................................................................
 *
 * @package   Client login
 * @author    Benjamin L.
 * @copyright 2015 Créatym <http://modules.creatym.fr>
 * @license   Commercial license
 * Support by mail  :  info@creatym.fr
 * Support on forum :  clientlogin
 * Phone : +33.87230110
 */

class ClientLoginHistory extends ObjectModel
{
	public $id;

	/** @var integer manufacturer ID //FIXME is it really usefull...? */
	public $id_customer;

	/** @var integer manufacturer ID //FIXME is it really usefull...? */
	public $id_employee;

	public $date_add;

	/**
	 * @see ObjectModel::$definition
	 */
	public static $definition = array(
		'table' => 'clientlogin_history',
		'primary' => 'id_clientlogin_history',
		'fields' => array(
			/* Classic fields */
			'id_customer' => 		array('type' => self::TYPE_INT,  'validate' => 'isInt'),
			'id_employee' => 		array('type' => self::TYPE_INT,  'validate' => 'isInt'),
			'date_add' =>           array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
		),
	);

	public static function eraseAllLogs()
    {
        return Db::getInstance()->execute('TRUNCATE TABLE '._DB_PREFIX_.'clientlogin_history');
    }

	public static function getHistory($limit = 3)
	{
		return Db::getInstance()->executeS('
			SELECT DISTINCT(a.id_customer), c.lastname, c.firstname, c.email
			FROM `'._DB_PREFIX_.'clientlogin_history` a
			LEFT JOIN `'._DB_PREFIX_.'customer` c ON (c.`id_customer` = a.`id_customer`)
			LEFT JOIN `'._DB_PREFIX_.'employee` e ON (e.`id_employee` = a.`id_employee`)
			WHERE 1
			ORDER BY a.`date_add` DESC LIMIT '.(int)$limit
		);
	}
}