<?php

require_once(dirname(__FILE__) . '/../../classes/HelFormClass.php');

class hel_formlandingModuleFrontController extends ModuleFrontController
{
    public $display_column_left = false;

    public function __construct()
    {
        parent::__construct();
        $this->context = Context::getContext();
    }

    public function initContent()
    {
        parent::initContent();

        $idForm = Tools::getValue("id");
        $db = Db::getInstance();
        $sql = "SELECT messageLandingPage FROM "._DB_PREFIX_."hel_form 
        WHERE id_hel_form=".$idForm;
        $res = $db->executeS($sql);

        $this->context->smarty->assign("message",nl2br($res[0]['messageLandingPage']));

        $urlReturn = Tools::getValue("url");
        $this->context->smarty->assign("urlReturn",$urlReturn);
        $this->setTemplate("landingPage.tpl");
    }
}