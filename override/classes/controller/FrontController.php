<?php

class FrontController extends FrontControllerCore
{

    public function postProcess()
    {
        if (Tools::getValue("has_hel_rgpd_traitement")) {
            require_once(_PS_MODULE_DIR_ . "hel_rgpd/hel_rgpd.php");
            Hel_rgpd::manageConsentementCustomer();
        }
        parent::postProcess();
    }
}
