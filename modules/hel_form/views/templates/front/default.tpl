{capture name=path}
    {l s="Formulaire" mod="hel_form"}
{/capture}

<link href="{$css_dir}global.css" rel="stylesheet">
<div>
    <ul id="contact">
        <image src="" class=""></image>
        <li>
            <span class="title">{l s="Pour participer, veuillez renseigner le formulaire ci-dessous." mod="hel_form"}</span>
        </li>
        <li>
            <form method="post" class="container" name="defaultForm" id="defaultForm">
                <fieldset>
                    <div>
                        <input type="text"  id="raisonSocial" name="raisonSocial" class="form-group" placeholder="{l s="Raison sociale" mod="hel_form"}"
                               value="{if isset($raisonSocial) && $raisonSocial ne ''}{$raisonSocial}{/if}"/>
                    </div>
                    <div>
                        <input type="text"  id="nom" name="nom" class="form-group" placeholder="{l s="Nom" mod="hel_form"}"
                               value="{if isset($nom) && $nom ne ''}{$nom}{/if}"/>
                    </div>
                    <div>
                        <input type="text"  id="prenom" name="prenom" class="form-group" placeholder="{l s="Prénom" mod="hel_form"}"
                               value="{if isset($prenom) && $prenom ne ''}{$prenom}{/if}"/>
                    </div>
                    <div>
                        <input type="email" id="email" name="email" class="form-group"  placeholder="{l s="Email" mod="hel_form"} *"
                               value = "{if isset($email_user)} {$email_user}{else}{if isset($email) && $email ne ''}{$email}{/if}{/if}" required/>
                    </div>
                    <div>
                        <input type="text"  id="ville" name="ville" class="form-group"  placeholder="{l s="Ville" mod="hel_form"}"
                               value="{if isset($ville) && $ville ne ''}{$ville}{/if}"/>
                    </div>
                     <div>
                        <label class="labelContact form-group">{l s="Message" mod="hel_form"}</label>
                        <textarea  id="message" name="message" class="textareaContact form-group" rows="8">{if isset($message) && $message ne ''}{$message}{/if}</textarea>
                    </div>
                    <input type="hidden" value="{if isset($activeCaptcha)} {$activeCaptcha}{/if}" name="activeCaptcha">
                    {if $activeCaptcha == 1 && $publicKey ne ''}
                        <div class="g-recaptcha" data-sitekey="{$publicKey}"></div>
                        {if isset($errorsCustom['captcha']) && $errorsCustom['captcha'] ne ''}
                            <span style="display: block; margin: 5px 0 20px 0; color: red; font-size: 12pt;">{$errorsCustom['captcha']}</span>
                        {/if}
                    {/if}
                    {hook h="displayHelRgpdTraitement" id_hel_rgpd_traitement="1"}
                    <div class="buttonSubmit">
                        <button type="submit" name="submitForm" id="submitForm" class="btn-default">
                            <span>{l s="Valider" mod="hel_form"}</span>
                        </button>
                    </div>
                </fieldset>
            </form>
        </li>
    </ul>
</div>
<script src='https://www.google.com/recaptcha/api.js'></script>