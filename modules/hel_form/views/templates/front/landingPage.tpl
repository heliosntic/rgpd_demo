{capture name=path}
    {l s="Formulaire confirmé" mod="hel_form"}
{/capture}

<link href="{$css_dir}global.css" rel="stylesheet">
<div>
    <form action="{$urlReturn}">
        <h3 style="text-align: center">{$message}</h3>
        <button type="submit" name="return" id="return" class="btn-default" style="display: block; margin-left: auto;
                margin-right: auto; margin-top: 40px; height: 55px; width: 200px">
            <span>{l s="Retour au site" mod="hel_form"}</span>
        </button>
    </form>
</div>