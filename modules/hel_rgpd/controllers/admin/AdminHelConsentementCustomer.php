<?php
/**
 * 2007-2014 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2014 PrestaShop SA
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

require_once(dirname(__FILE__) . '/../../hel_rgpd.php');
require_once(dirname(__FILE__) . '/../../classes/HelConsentementCustomer.php');

class AdminHelConsentementCustomerController extends ModuleAdminController
{

    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'hel_rgpd_consentement_customer';
        $this->className = 'HelConsentementCustomer';

        $this->_select = 'nameTraitement,CONCAT(COALESCE(c.lastname,\'\'),\' \',CONCAT(COALESCE(c.firstname,\'\')),\' \',COALESCE(c.company,\'\'),\' (\',c.id_customer,\')\') as customer_name';
        $this->_join = '
            LEFT JOIN `' . _DB_PREFIX_ . 'customer` c ON (c.`id_customer` = a.`id_customer`)
            INNER JOIN `' . _DB_PREFIX_ . 'hel_rgpd_traitement` d ON (d.`id_hel_rgpd_traitement` = a.`id_hel_rgpd_traitement`)
            ';

        $this->fields_list = array(
            'nameTraitement' => array(
                'title' => $this->l('Traitement'),
                'align' => 'center',
                'class' => 'fixed-width-xs',
                'search' => true,
                'remove_onclick' => true,
            ),
            'customer_name' => array(
                'title' => $this->l('Client'),
                'search' => false,
                'remove_onclick' => false,
            ),
            'identifiant' => array(
                'title' => $this->l('Identifiant client'),
                'search' => false,
                'remove_onclick' => false,
            ),
            'decision' => array(
                'title' => $this->l('Décision'),
                'search' => true,
                'callback'=> 'printDecision',
                'remove_onclick' => true,
            ),
            'date_add' => array(
                'title' => $this->l('Ajouté le'),
                'search' => true,
                'remove_onclick' => true,
            ),
            'date_upd' => array(
                'title' => $this->l('Modifié le'),
                'search' => true,
                'remove_onclick' => true,
            ),
        );

        parent::__construct();
    }

    public function printDecision($value){
        if($value == 1){
            return "Oui";
        }
        else{
            return "Non";
        }
    }



}
