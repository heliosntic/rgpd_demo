<?php
/**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_')) {
    exit;
}
require_once(dirname(__FILE__) . '/classes/HelTraitement.php');
require_once(dirname(__FILE__) . '/classes/HelConsentementCustomer.php');
require_once(dirname(__FILE__) . '/classes/WebserviceSpecificManagementHelRpgdConsentementCustomer.php');

class Hel_rgpd extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'hel_rgpd';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'Helios NTIC';
        $this->need_instance = 0;
        $this->controllers = array("traitementDetail", "listConsentementCustomer");

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Règlement Général sur la Protection des Données');
        $this->description = $this->l('Le RGPD (ou GDPR) est le Règlement Général sur la Protection des Données, une nouvelle réglementation européenne qui entrera en vigueur le 25 mai 2018.');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        include(dirname(__FILE__) . '/sql/install.php');

        $scss_path = dirname(__FILE__) . '/views/sass/hel_rgpd.scss';
        $css_path = dirname(__FILE__) . '/views/css/hel_rgpd.css';
        $theme_scss_dir = _PS_THEME_DIR_ . 'sass/modules/hel_rgpd/views/css/hel_rgpd.scss';
        $theme_css_dir = _PS_THEME_DIR_ . 'css/modules/hel_rgpd/views/css/hel_rgpd.css';

        if (file_exists($scss_path) && !file_exists($theme_scss_dir . '/hel_rgpd.scss')) {
            if (!is_dir(dirname($theme_scss_dir))) {
                mkdir(_PS_THEME_DIR_ . 'sass/modules/hel_rgpd');
                mkdir(_PS_THEME_DIR_ . 'sass/modules/hel_rgpd/views');
                mkdir(_PS_THEME_DIR_ . 'sass/modules/hel_rgpd/views/css');
            }
            copy($scss_path, $theme_scss_dir);
        }
        if (file_exists($css_path) && !file_exists($theme_css_dir . '/hel_rgpd.scss')) {
            if (!is_dir(dirname($theme_css_dir))) {
                mkdir(_PS_THEME_DIR_ . 'css/modules/hel_rgpd');
                mkdir(_PS_THEME_DIR_ . 'css/modules/hel_rgpd/views');
                mkdir(_PS_THEME_DIR_ . 'css/modules/hel_rgpd/views/css');
            }
            copy($css_path, $theme_css_dir);
        }

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('displayHelRgpdTraitement') &&
            $this->registerHook('webServiceGetResources') &&
            $this->registerHook('customerAccount') &&
            $this->registerHook('registerGDPRConsent') &&
            $this->registerHook('actionExportGDPRData') &&
            $this->registerHook('actionDeleteGDPRCustomer') &&
            $this->registerHook('displayPowerfulFormInForm')
            && $this->installAdminTab('AdminHelTraitementTab', $this->l('RGPD'), '')
            && $this->installAdminTab('AdminHelTraitement', $this->l('Traitements'), 'AdminHelTraitementTab')
            && $this->installAdminTab('AdminHelConsentementCustomer', $this->l('Consentement Client'), 'AdminHelTraitementTab');
    }

    public function uninstall()
    {
        include(dirname(__FILE__) . '/sql/uninstall.php');

        return parent::uninstall()
            && $this->uninstallAdminTab('AdminHelTraitementTab')
            && $this->uninstallAdminTab('AdminHelTraitement')
            && $this->uninstallAdminTab('AdminHelConsentementCustomer');
    }


    public function installAdminTab($class, $name, $parent_tab)
    {
        $tabParent = intval(Db::getInstance()->getValue('SELECT `id_tab` FROM `' . _DB_PREFIX_ . 'tab` WHERE `class_name` = "' . $parent_tab . '"'));

        //$this->copyLogo($class, 't/');

        $tab = new Tab();

        foreach (Language::getLanguages() as $language)
            $tab->name[$language['id_lang']] = $name;

        $tab->class_name = $class;
        $tab->module = $this->name;
        $tab->id_parent = $tabParent;
        $tab->position = 1;

        //if PS 1.5 remove tab class
        if ((float)_PS_VERSION_ >= 1.5 && file_exists(dirname(__FILE__) . '/../' . $class . '.php'))
            rename(dirname(__FILE__) . '/../' . $class . '.php', dirname(__FILE__) . '/../_' . $class . '.php');

        return $tab->save();
    }

    public function uninstallAdminTab($class)
    {
        if ($idTab = Tab::getIdFromClassName($class)) {
            //$this->deleteLogo($class, 't/');
            $tab = new Tab($idTab);
            $tab->delete();

            //if PS 1.5 return tab class
            if ((float)_PS_VERSION_ >= 1.5 && file_exists(dirname(__FILE__) . '/../_' . $class . '.php'))
                rename(dirname(__FILE__) . '/../_' . $class . '.php', dirname(__FILE__) . '/../' . $class . '.php');

            return true;
        }
        return true;
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
//        if (((bool)Tools::isSubmit('submitHel_rgpdModule')) == true) {
//            $this->postProcess();
//        }

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/configure.tpl');

//        return $output . $this->renderForm();
        return $output;
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
//    protected function renderForm()
//    {
//        $helper = new HelperForm();
//
//        $helper->show_toolbar = false;
//        $helper->table = $this->table;
//        $helper->module = $this;
//        $helper->default_form_language = $this->context->language->id;
//        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);
//
//        $helper->identifier = $this->identifier;
//        $helper->submit_action = 'submitHel_rgpdModule';
//        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
//            . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
//        $helper->token = Tools::getAdminTokenLite('AdminModules');
//
//        $helper->tpl_vars = array(
//            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
//            'languages' => $this->context->controller->getLanguages(),
//            'id_language' => $this->context->language->id,
//        );
//
//        return $helper->generateForm(array($this->getConfigForm()));
//    }

    /**
     * Create the structure of your form.
     */
//    protected function getConfigForm()
//    {
//        return array(
//            'form' => array(
//                'legend' => array(
//                    'title' => $this->l('Settings'),
//                    'icon' => 'icon-cogs',
//                ),
//                'input' => array(
//                    array(
//                        'col' => 3,
//                        'type' => 'text',
//                        'prefix' => '<i class="icon icon-envelope"></i>',
//                        'desc' => $this->l('Enter a valid email address'),
//                        'name' => 'HEL_RGPD_ACCOUNT_EMAIL',
//                        'label' => $this->l('Email'),
//                    ),
//                    array(
//                        'type' => 'password',
//                        'name' => 'HEL_RGPD_ACCOUNT_PASSWORD',
//                        'label' => $this->l('Password'),
//                    ),
//                ),
//                'submit' => array(
//                    'title' => $this->l('Save'),
//                ),
//            ),
//        );
//    }
//
//    /**
//     * Set values for the inputs.
//     */
//    protected function getConfigFormValues()
//    {
//        return array(
//            'HEL_RGPD_ACCOUNT_EMAIL' => Configuration::get('HEL_RGPD_ACCOUNT_EMAIL', 'contact@prestashop.com'),
//            'HEL_RGPD_ACCOUNT_PASSWORD' => Configuration::get('HEL_RGPD_ACCOUNT_PASSWORD', null),
//        );
//    }
//
//    /**
//     * Save form data.
//     */
//    protected function postProcess()
//    {
//        $form_values = $this->getConfigFormValues();
//
//        foreach (array_keys($form_values) as $key) {
//            Configuration::updateValue($key, Tools::getValue($key));
//        }
//    }

    /**
     * Add the CSS & JavaScript files you want to be loaded in the BO.
     */
    public function hookDisplayHelRgpdTraitement($params)
    {
//        var_dump($params);
        $hel_rgpd_traitement = new HelTraitement($params["id_hel_rgpd_traitement"], $this->context->language->id);
        Media::addJsDef(array("messageSiObligatoire" . $params["id_hel_rgpd_traitement"] => str_replace("'", "\'", $hel_rgpd_traitement->messageSiObligatoire)));
        $hel_rgpd_consentement_customer = null;
        if ($hel_rgpd_traitement->afficheStateInForm == true && isset($this->context->customer) && $this->context->customer->id != null) {
            $hel_rgpd_consentement_customer = HelConsentementCustomer::getLastConsentementsCustomer($this->context->customer->id, '', $hel_rgpd_traitement->id);
        }
        $this->context->smarty->assign(array(
            'hel_rgpd_traitement' => $hel_rgpd_traitement,
            'hel_rgpd_consentement_customer' => $hel_rgpd_consentement_customer,
        ));
        
        if (Module::_isTemplateOverloadedStatic($this->name, "hel_rgpd_traitement_form_" . $hel_rgpd_traitement->codeTraitement . ".tpl") != null) {
            return $this->display(__FILE__, "hel_rgpd_traitement_form_" . $hel_rgpd_traitement->codeTraitement . ".tpl");
        }

        return $this->display(__FILE__, 'hel_rgpd_traitement_form.tpl');
    }

    public function hookDisplayPowerfulFormInForm($params)
    {
        $traitements = HelTraitement::getByAllPFGidForm($params["idPfg"]);
        //var_dump($traitements);
        $res_html = "";
        foreach ($traitements as $traitement) {
            $params["id_hel_rgpd_traitement"] = $traitement;
            $res_html .= $this->hookDisplayHelRgpdTraitement($params);
        }

        return $res_html;
    }


    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addCSS($this->_path . '/views/css/hel_rgpd.css');

        self::manageConsentementCustomer();
    }

    public static function manageConsentementCustomer()
    {
        if (Tools::getValue("has_hel_rgpd_traitement")) {
            require_once(_PS_MODULE_DIR_ . "hel_rgpd/hel_rgpd.php");
            $array_hel_rgpd_traitement_values = array_filter($_REQUEST, function ($key) {
                return strpos($key, 'hel_rgpd_traitement_value_') === 0;
            }, ARRAY_FILTER_USE_KEY);
            foreach ($array_hel_rgpd_traitement_values as $index => $array_hel_rgpd_traitement_value) {

//                public static function addConsentement($hel_rgpd_traitement_id,$hel_rgpd_traitement_value, $identifiant="", $origin=""){
                $array_hel_rgpd_traitement_value = explode("|", $array_hel_rgpd_traitement_value);
                $hel_rgpd_traitement_id = $array_hel_rgpd_traitement_value[0];
                $hel_rgpd_traitement_value = $array_hel_rgpd_traitement_value[1];
                $hel_traitement = new HelTraitement($hel_rgpd_traitement_id);

                $champ_identifiant_array = explode(";", $hel_traitement->champ_identifiant);
                $identifiant = "";
                foreach ($champ_identifiant_array as $champ_identifiant) {
                    if (Tools::getValue($champ_identifiant)) {
                        $identifiant .= Tools::getValue($champ_identifiant) . ";";
                    }
                }
                $identifiant = substr($identifiant, 0, strlen($identifiant) - 1);

                Hel_rgpd::addConsentement($hel_traitement, $hel_rgpd_traitement_value, $identifiant, 'form');
                unset($_POST["hel_rgpd_traitement_value_1"]);
            }
        }
    }


    public function hookCustomerAccount($params)
    {
        $current_customer = new Customer($this->context->customer->id);
        $this->context->smarty->assign(array(
            'current_customer' => $current_customer));

        return $this->display(__FILE__, 'my-account.tpl');
    }


    public static function addConsentement($hel_traitement, $hel_rgpd_traitement_value, $identifiant = "", $origin = "")
    {

        $context = Context::getContext();

//        krumo($context->customer);
//        krumo($_REQUEST);
//        krumo($context);
//        exit();

        $hel_consentement_customer = new HelConsentementCustomer(null, $context->language->id, $context->shop->id);
        $hel_consentement_customer->id_hel_rgpd_traitement = $hel_traitement->id;
        $hel_consentement_customer->active = true;
        $hel_consentement_customer->origin = $origin;

        $date_validity = new DateTime();
        $date_validity->add(new DateInterval('P' . $hel_traitement->duree . 'D'));
        $hel_consentement_customer->date_validity = $date_validity->format("Y-m-d");

        $hel_consentement_customer->origin_referrer = $_SERVER["HTTP_REFERER"];
        if (isset($context->customer->id) && $context->customer->id != null) {
            $hel_consentement_customer->id_customer = $context->customer->id;
        }

        if ($hel_traitement->champ_identifiant != "") {
            if ($hel_consentement_customer->id_customer == null && Validate::isEmail($identifiant)) {
                /* Le client n'est pas connecté mais on a son mail, on essaye donc de retrouver le client correspondant */
                $customers = Customer::getCustomersByEmail(Tools::getValue($identifiant));

                if (count($customers) > 0) {
                    $hel_consentement_customer->id_customer = $customers[0]["id_customer"];
                }
            }

            $hel_consentement_customer->identifiant = $identifiant;

        } else {
            $hel_consentement_customer->identifiant = $identifiant;

        }


        $hel_consentement_customer->decision = $hel_rgpd_traitement_value;
        HelConsentementCustomer::unactiveConsentementCustomer($hel_consentement_customer->id_hel_rgpd_traitement, $hel_consentement_customer->id_customer, $hel_consentement_customer->identifiant);
        $hel_consentement_customer->add();
        return $hel_consentement_customer;

    }

    public function hookWebServiceGetResources($params)
    {
        $params["resources"]["hel_rpgd_consentement_customer"] = array('description' => 'Consentement des clients au traitements (RGPD)', 'class' => 'HelConsentementCustomer', 'specific_management' => true);
        ksort($params["resources"]);
    }

    public function hookRegisterGDPRConsent($params)
    {
        return true;
    }


    public function hookActionExportGDPRData($customer)
    {

        $res = HelConsentementCustomer::getConsentementsCustomer($customer["id"]);
        foreach ($res as $key => $line) {
            $line_final = array();
//            krumo($line);
            $traitement = new HelTraitement($line["id_hel_rgpd_traitement"]);
            $line_final["Traitement"] = $traitement->nameTraitement;
            $line_final["Identifiant"] = $line["identifiant"];
            $line_final["Consentement"] = ($line["decision"] == 1) ? "Oui" : "Non";
            $line_final["Actif"] = ($line["active"] == 1) ? "Oui" : "Non";
            $line_final["Date_ajout"] = $line["date_add"];
            $line_final["Date_validite"] = $line["date_validity"];
//            krumo($line);
            $res[$key] = $line_final;
        }
        return Tools::jsonEncode($res);
    }

    public function hookActionDeleteGDPRCustomer($params)
    {

        if (!empty($customer['email']) && Validate::isEmail($customer['email'])) {
            $sql = "DELETE FROM " . _DB_PREFIX_ . "hel_rgpd_consentement_customer WHERE identifiant = '" . pSQL($customer['email']) . "' OR id_customer=" . pSQL($customer['id']) . "";
            if (Db::getInstance()->execute($sql)) {
                return json_encode(true);
            }
            return json_encode($this->l('Newsletter Popup : Unable to delete customer using email.'));
        }
    }


}
