<?php
/**
 * 2007-2014 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2014 PrestaShop SA
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

require_once(dirname(__FILE__) . '/../../hel_rgpd.php');
require_once(dirname(__FILE__) . '/../../classes/HelTraitement.php');

class AdminHelTraitementController extends ModuleAdminController
{

    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'hel_rgpd_traitement';
        $this->className = 'HelTraitement';
        $this->lang = false;
//        $this->explicitSelect = false;
        $this->_select = 'id_hel_rgpd_traitement AS emplacement';

        $this->fields_list = array(
            'id_hel_rgpd_traitement' => array(
                'title' => $this->l('ID'),
                'align' => 'center',
                'class' => 'fixed-width-xs',
                'search' => true,
            ),
            'nameTraitement' => array(
                'title' => $this->l('Nom'),
                'search' => true,
            ),
            'active' => array(
                'title' => $this->l('Actif ?'),
                'class' => 'fixed-width-sm',
                'align' => 'center',
                'active' => 'status',
                'type' => 'bool',
                'orderby' => false
            ),
            'obligatoire' => array(
                'title' => $this->l('Obligatoire ?'),
                'class' => 'fixed-width-sm',
                'align' => 'center',
                'active' => 'obligatoire',
                'type' => 'bool',
                'orderby' => false
            )
        );

        parent::__construct();
    }


    public function printInfosSuppl($value)
    {
        $context = Context::getContext();
        $hel_Traitement = new HelTraitement($value);

        $msg = "<div class=\"panel\"><div class=\"panel-heading\">Emplacement du traitement</div>";
        $msg .= "<p>Veuillez intégrer le code suivant à l'endroit où vous voulez voir s'afficher le consentement dans le formulaire : <br/>{hook h=\"displayHelRgpdTraitement\" id_hel_rgpd_traitement=\"".$value."\"}</p>";
        $msg .= "<p>Lien de désabonnement pour un client : &lt;a href=\"".$context->shop->getBaseURL()."module/hel_rgpd/unsubscribeTraitement?id_customer=##ID_CLIENT##&&codeTraitement=".$hel_Traitement->codeTraitement."&cookie_key=".md5($hel_Traitement->cookie_key.$hel_Traitement->codeTraitement)."\"&gt;Vous désabonner&lt;/a&gt;</p>";
        $msg .= "<p>Lien de désabonnement pour un non client : &lt;a href=\"".$context->shop->getBaseURL()."module/hel_rgpd/unsubscribeTraitement?email_customer=##EMAIL##&&codeTraitement=".$hel_Traitement->codeTraitement."&cookie_key=".md5($hel_Traitement->cookie_key.$hel_Traitement->codeTraitement)."\"&gt;Vous désabonner&lt;/a&gt;</p>";
        if(Module::isInstalled("powerfulformgenerator") && Module::isEnabled("powerfulformgenerator")){
            $msg .= "<p>Pour le module PowerFullFormGenerator, ajoutez la ligne suivante à la ligne 53 du fichier modules/powerfulformgenerator/views/templates/front/form-1.6.tpl avant de renseigner les id des formulaires concernés dans le traitement : <br/>{hook h='displayPowerfulFormInForm' idPfg=\$idPfg}</p>";
        }
        $msg .= "<p>Pour avoir un template spécifique d'affichage dans le formulaire, il suffit de créer le fichier suivant : themes/##NOM_DU_THEME##/modules/hel_rgpd/views/templates/front/hel_rgpd_traitement_form_".$hel_Traitement->codeTraitement.".tpl </p>";
        return $msg."</div>";
    }



	public function renderList()
    {

        $this->addRowAction('edit');
//        $this->addRowAction('delete');

        return parent::renderList();
    }

    public function renderForm()
    {
//        $mode_synchro_list = array(
//            array(
//                'id' => 'full',
//                'name' => $this->l('Complète')
//            ),
//            array(
//                'id' => 'incr',
//                'name' => $this->l('Incrémentale')
//            ),
//        );

        $this->fields_form = array(
            'legend' => array(
                'title' => $this->l('Job')
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Nom'),
                    'name' => 'nameTraitement',
                    'required' => false,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Code du traitement'),
                    'name' => 'codeTraitement',
                    'desc' => 'Code du traitement sans espace ni caractères spéciaux',
                    'required' => true,
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Description affichée sur le formulaire'),
                    'name' => 'description',
                    'desc' => "Vous pouvez utiliser le jeton ##finaliteTraitement## pour afficher automatiquement la finalité du traitement dans la description du consentement",
                    'required' => true,
                    'class' => 'rte',
                    'autoload_rte' => true,
                    'lang' => true,
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Finalité du traitement'),
                    'name' => 'finaliteTraitement',
                    'required' => true,
                    'class' => 'rte',
                    'autoload_rte' => true,
                    'lang' => true,
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Types de données'),
                    'name' => 'typesDonnees',
                    'required' => true,
                    'class' => 'rte',
                    'autoload_rte' => true,
                    'lang' => true,
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Transfert de données à des tiers'),
                    'name' => 'transfertsDonnees',
                    'required' => true,
                    'class' => 'rte',
                    'autoload_rte' => true,
                    'lang' => true,
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Procédure de retrait du consentement'),
                    'name' => 'procedureRetrait',
                    'required' => true,
                    'class' => 'rte',
                    'autoload_rte' => true,
                    'lang' => true,
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Responsable du traitement des données'),
                    'name' => 'responsableTraitement',
                    'required' => true,
                    'class' => 'rte',
                    'autoload_rte' => true,
                    'lang' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Durée de validité du traitement en jours'),
                    'name' => 'duree',
                    'required' => false,
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Prise de décision automatisée O/N'),
                    'name' => 'decisionAutomatisee',
                    'required' => false,
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'decisionAutomatisee_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'decisionAutomatisee_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Consentement O/N'),
                    'name' => 'hasConsentement',
                    'required' => true,
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'hasConsentement_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'hasConsentement_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Consentement pré-approuvé'),
                    'name' => 'consentementPreApprouved',
                    'desc' => "A cocher si un des autres éléments du formulaire vaut pour approuvement du consentement",
                    'required' => true,
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'consentementPreApprouved_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'consentementPreApprouved_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Réponse obligatoire O/N'),
                    'name' => 'obligatoire',
                    'required' => true,
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'obligatoire_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'obligatoire_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Message à afficher si le champ est obligatoire'),
                    'name' => 'messageSiObligatoire',
                    'required' => false,
                    'lang' => true,
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Actif O/N'),
                    'name' => 'active',
                    'required' => true,
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )

                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Afficher l\'état du consentement dans le formulaire O/N'),
                    'name' => 'afficheStateInForm',
                    'required' => true,
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'afficheStateInForm_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'afficheStateInForm_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Nom du champ identifiant le client'),
                    'name' => 'champ_identifiant',
                    'desc' => "Nom du(des) champ(s) dans le formulaire, s'il y a plusieurs noms, séparez les par des ;",
                    'required' => false,
                ),

            )
        );


        if (Shop::isFeatureActive())
        {
            $this->fields_form['input'][] = array(
                'type' => 'shop',
                'label' => $this->l('Shop association'),
                'hint' => $this->l('Select the shops the employee is allowed to access.'),
                'name' => 'checkBoxShopAsso',
            );
        }

        if(Module::isInstalled("powerfulformgenerator") && Module::isEnabled("powerfulformgenerator")){
            $this->fields_form['input'][] = array(
                'type' => 'text',
                'label' => $this->l('Id des formulaires (PFG)'),
                'name' => 'PFGidForms',
                'desc' => "ID du(des) formulaire(s) du module PowerFullFormGenerator, s'il y a plusieurs id, séparez les par des ;",
                'required' => false,
            );
        }


        $this->fields_form['submit'] = array(
            'title' => $this->l('Save'),

        );
        $html_render = "";
        if($id_hel_rgpd_traitement = Tools::getValue("id_hel_rgpd_traitement")){
            $html_render.=$this->printInfosSuppl($id_hel_rgpd_traitement);
        }
        return $html_render.parent::renderForm();
    }

    public function postProcess()
    {
        parent::postProcess();
    }


}
