<?php

/**
 * Created by PhpStorm.
 * User: m.anceaux
 * Date: 05/03/2018
 * Time: 15:07
 */
class HelTraitement extends ObjectModel
{

    public $id_hel_rgpd_traitement;
    public $nameTraitement;
    public $finaliteTraitement;
    public $typesDonnees;
    public $transfertsDonnees;
    public $procedureRetrait;
    public $decisionAutomatisee;
    public $responsableTraitement;
    public $active;
    public $duree;
    public $obligatoire;
    public $champ_identifiant;
    public $description;
    public $messageSiObligatoire;
    public $codeTraitement;
    public $afficheStateInForm;
    public $PFGidForms;
    public $hasConsentement;
    public $cookie_key;
//    public $emplacement;

    public static $definition = array(
        'table' => 'hel_rgpd_traitement',
        'primary' => 'id_hel_rgpd_traitement',
        'multilang' => true,
        'fields' => array(
            'nameTraitement' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'lang' => false, 'required' => true, 'size' => 255),
            'description' => array('type' => self::TYPE_HTML, 'validate' => 'isCleanHtml', 'lang' => true, 'required' => true, 'size' => 1024),
            'finaliteTraitement' => array('type' => self::TYPE_HTML, 'validate' => 'isCleanHtml', 'lang' => true, 'required' => true, 'size' => 1024),
            'typesDonnees' => array('type' => self::TYPE_HTML, 'validate' => 'isCleanHtml', 'lang' => true, 'required' => true, 'size' => 1024),
            'transfertsDonnees' => array('type' => self::TYPE_HTML, 'validate' => 'isCleanHtml', 'lang' => true, 'required' => true, 'size' => 1024),
            'procedureRetrait' => array('type' => self::TYPE_HTML, 'validate' => 'isCleanHtml', 'lang' => true, 'required' => true, 'size' => 1024),
            'decisionAutomatisee' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
            'responsableTraitement' => array('type' => self::TYPE_HTML, 'validate' => 'isCleanHtml', 'lang' => true, 'required' => true, 'size' => 1024),
            'active' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
            'obligatoire' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
            'afficheStateInForm' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
            'duree' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'champ_identifiant' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'lang' => false, 'required' => false, 'size' => 255),
            'messageSiObligatoire' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'lang' => true, 'required' => false, 'size' => 255),
            'codeTraitement' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'lang' => false, 'required' => true, 'size' => 20),
            'PFGidForms' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'lang' => false, 'required' => false, 'size' => 255),
            'hasConsentement' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'lang' => false, 'required' => true),
            'consentementPreApprouved' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'lang' => false, 'required' => true),
            'cookie_key' => array('type' => self::TYPE_STRING,'size' => 255),
            /*'emplacement' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true, 'size' => 255),*/
        ),
    );

    public function add($autodate = true, $null_values = false)
    {
        $this->cookie_key = Tools::passwdGen(50);
        return parent::add($autodate, true);
    }

    public function update($null_values = false)
    {
        if($this->cookie_key == ""){
            $this->cookie_key = Tools::passwdGen(50);

        }
        return parent::update($null_values);
    }

    public function delete()
    {
        if (parent::delete())
            return true;
        return false;
    }

    public static function getByCode($codeTraitement, $mode_return = "object")
    {
        $id_traitement = Db::getInstance()->getValue("SELECT id_hel_rgpd_traitement FROM " . _DB_PREFIX_ . "hel_rgpd_traitement WHERE codeTraitement='" . $codeTraitement . "'");
        if ($mode_return == "object") {
            return new HelTraitement($id_traitement);
        } else {
            return $id_traitement;
        }

    }


    public static function getByAllPFGidForm($PFGidForm)
    {
        $res = Db::getInstance()->executeS("SELECT id_hel_rgpd_traitement,PFGidForms FROM " . _DB_PREFIX_ . "hel_rgpd_traitement");
        $lst_traitements = array();
        foreach ($res as $hel_rgpd_traitement) {
           $PFGidForms = explode(";",$hel_rgpd_traitement["PFGidForms"]);
           if(in_array($PFGidForm,$PFGidForms)){
               $lst_traitements[] = $hel_rgpd_traitement["id_hel_rgpd_traitement"];
           }
        }
        return $lst_traitements;

    }


}