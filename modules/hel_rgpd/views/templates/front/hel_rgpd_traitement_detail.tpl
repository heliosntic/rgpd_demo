{capture name=path}{$name}{/capture}
{capture name=title}{$name}{/capture}
<h1>{$hel_rgpd_traitement->nameTraitement}</h1>
<div id="hel_rgpd" class="row">
    <div class="col-sm-6">
        <div class="block">
            <div class="title">{l s='FINALITÉ DU TRAITEMENT DE VOS DONNÉES PERSONNELLES' mod='hel_rgpd'}</div>
            <div class="content">{$hel_rgpd_traitement->finaliteTraitement}</div>
        </div>
        <div class="block">
            <div class="title">{l s='TYPES DE DONNÉES' mod='hel_rgpd'}</div>
            <div class="content">{$hel_rgpd_traitement->typesDonnees}</div>
        </div>
        <div class="block">
            <div class="title">{l s='TRANSFERT DE DONNÉES À DES TIERS' mod='hel_rgpd'}</div>
            <div class="content">{$hel_rgpd_traitement->transfertsDonnees}</div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="block">
            <div class="title">{l s='PROCÉDURE DE RETRAIT DU CONSENTEMENT' mod='hel_rgpd'}</div>
            <div class="content">{$hel_rgpd_traitement->procedureRetrait}</div>
        </div>
        <div class="block">
            <div class="title">{l s='PRISE DE DÉCISIONS AUTOMATISÉE' mod='hel_rgpd'}</div>
            <div class="content">
                {if $hel_rgpd_traitement->decisionAutomatisee == 1}
                    {l s='Ces données personnelles sont utilisées pour de la prise de décision automatisée' mod='hel_rgpd'}
                {else}
                    {l s='Ces données personnelles ne sont pas utilisées pour de la prise de décision automatisée' mod='hel_rgpd'}
                {/if}
            </div>
        </div>
        <div class="block">
            <div class="title">{l s='RESPONSABLE DU TRAITEMENT DES DONNÉES' mod='hel_rgpd'}</div>
            <div class="content">{$hel_rgpd_traitement->responsableTraitement }</div>
        </div>
    </div>
</div>