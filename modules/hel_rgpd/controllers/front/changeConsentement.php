<?php
/**
 * Created by PhpStorm.
 * User: b.bonnin
 * Date: 03/04/2018
 * Time: 09:49
 */

require_once(dirname(__FILE__) . '/../../classes/HelTraitement.php');
require_once(dirname(__FILE__) . '/../../classes/HelConsentementCustomer.php');

class hel_rgpdchangeConsentementModuleFrontController extends ModuleFrontController
{
    public $auth = false;
    public $ssl = true;

    public function __construct()
    {
        parent::__construct();
        $this->ajax = true;
        $this->context = Context::getContext();
    }

    public function initContent()
    {
        parent::initContent();
        $customer_id = Tools::getValue("customer_id");
        $identifiant = Tools::getValue("identifiant");
//        if(Tools::getValue("decision") == 1){
//            $decision ="N" ;
//        }
//        else{
//            $decision ="Y" ;
//        }

//        krumo($customer_id);
//        krumo($identifiant);
        $hel_rgpd_traitement_id = Tools::getValue("hel_rgpd_traitement_id");
        $hel_traitement = new HelTraitement($hel_rgpd_traitement_id);
        $last_decision = HelConsentementCustomer::getLastConsentementsCustomer($customer_id, $identifiant, $hel_rgpd_traitement_id);
        if ($identifiant == null) {
            $identifiant = $last_decision->identifiant;
        }
        if ($last_decision->decision == 0) {
            $decision = 1;
        } else {
            $decision = 0;
        }

        HelConsentementCustomer::unactiveConsentementCustomer($hel_rgpd_traitement_id, $customer_id, $identifiant);
        $hel_consentement_customer = Hel_rgpd::addConsentement($hel_traitement, $decision, $identifiant, 'my-account');


        die(Tools::jsonEncode(array("hel_consentement_customer" => $hel_consentement_customer)));
    }
}