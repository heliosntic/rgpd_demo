{if $id_customer != 0}

    {if count($hel_rgpd_consentements_customer)}
        <div id="hel_rgpd" class="row">
            {foreach from=$hel_rgpd_consentements_customer item=hel_rgpd_consentement_customer name=myLoop}
                <div class="col-sm-6">
                    <div class="block">
                        <div class="hel_rgpd_traitement title"
                             id="hel_rgpd_traitement{$hel_rgpd_consentement_customer["traitement"]->id}">
                            <span>
                                <h3>{l s='Traitement' mod='hel_rgpd'}
                                    : {$hel_rgpd_consentement_customer["traitement"]->nameTraitement}
                                    {if $hel_rgpd_consentement_customer["traitement_last_decision"] == 1}
                                        <i class="icon-check-circle green"></i>
                                    {elseif $hel_rgpd_consentement_customer["traitement_last_decision"] == 0}
                                        <i class="icon-times-circle red"></i>
                                    {/if}
                                </h3>
                            </span>
                            <span class="change_consent">
                                {l s="Changer le consentement"}
                                    <a href="#" data-traitement="{$hel_rgpd_consentement_customer["traitement"]->id}"
                                       data-customer="{$id_customer}"
                                       data-decision="{$hel_rgpd_consentement_customer["traitement_last_decision"]}"
                                       id="change_consentement{$hel_rgpd_consentement_customer["traitement"]->id}"
                                       class="change_consentement"
                                       id="change_consentement{$hel_rgpd_consentement_customer["traitement"]->id}">
                                <label class="switch">
                                    <div {if $hel_rgpd_consentement_customer["traitement_last_decision"] == 1}class="checked"{/if}></div>
                                    <span class="slider round"></span>
                                </label></a>
                            </span>
                            {*<span id="traitement_last_decision{$hel_rgpd_consentement_customer["traitement"]->id}">{$hel_rgpd_consentement_customer["traitement_last_decision"]}</span>*}
                        </div>

                        <div class="hel_rgpd_content">
                            <div>{l s='Date de dernière mise à jour :' mod='hel_rgpd'} <span id="last_upd{$hel_rgpd_consentement_customer["traitement"]->id}">{$hel_rgpd_consentement_customer["traitement_last_decision_date"]|date_format:"%d/%m/%Y %k:%M"}</span></div>
                            <div>
                                <a href="{$link->getModuleLink("hel_rgpd","traitementDetail",["hel_rgpd_traitement_id"=>$hel_rgpd_consentement_customer["traitement"]->id])}"
                                   target="_blank">{l s='Consulter les détails du traitement' mod='hel_rgpd'}</a>
                            </div>
                            <button data-toggle="collapse"
                                    data-target="#hel_rgpd_consentement_customer{$hel_rgpd_consentement_customer["traitement"]->id}"
                                    class="btn btn-default">{l s="Voir l'historique des consentements" mod='hel_rgpd'}</button>
                            <div class="hel_rgpd_consentement_customer collapse"
                                 id="hel_rgpd_consentement_customer{$hel_rgpd_consentement_customer["traitement"]->id}">
                                {*{l s='Liste de vos consentements' mod='hel_rgpd'} :*}
                                {assign var="nbPages" value=count($hel_rgpd_consentement_customer["consentements_customer"])/10|ceil}
                                <input type="hidden" id="num_page" name="num_page" value="1">
                                <input type="hidden" id="id_traitement" name="id_traitement" value="{$hel_rgpd_consentement_customer["traitement"]->id}">
                                {if $nbPages > 1}
                                <ul class="nav nav-tabs">
                                {for $page=1 to $nbPages}
                                    {*<button class="page" data-target="#page_{$hel_rgpd_consentement_customer["traitement"]->id}_{$page}" data-toggle="tab">{$page}</button>*}
                                    <li {if $page == 1}class="active"{/if}><a data-toggle="tab" href="#page_{$hel_rgpd_consentement_customer["traitement"]->id}_{$page}">{$page}</a></li>
                                {/for}
                                </ul>
                                {/if}
                                <table class="table"
                                       id="hel_rgpd_consentement_customer_list{$hel_rgpd_consentement_customer["traitement"]->id}">
                                    {*<div >*}
                                    <thead>
                                    <tr>
                                        <th scope="col">{l s='Date' mod='hel_rgpd'}</th>
                                        <th scope="col">{l s='Décision' mod='hel_rgpd'}</th>
                                    </tr>
                                    </thead>
                                    {foreach from=$hel_rgpd_consentement_customer["consentements_customer"] item=hel_rgpd_consentement_customer_detail name=myLoop}
                                    {if $smarty.foreach.myLoop.index % 10 == 0}
                                        <tbody class="{if $smarty.foreach.myLoop.index/10 == 0}active in{/if} fade" id="page_{$hel_rgpd_consentement_customer["traitement"]->id}_{math equation="x/y + z" x=$smarty.foreach.myLoop.index y=10 z=1}">
                                    {/if}
                                        <tr class="hel_rgpd_consentement_customer_detail">
                                            {*{l s='Date du consentement' mod='hel_rgpd'} : *}
                                            <td>{$hel_rgpd_consentement_customer_detail["date_add"]|date_format:"%d/%m/%Y %k:%M"}</td>
                                            {*{l s='Décision' mod='hel_rgpd'} :*}
                                            <td>
                                                {if $hel_rgpd_consentement_customer_detail["decision"] == 1}
                                                    <i class="icon-check-circle green"></i>
                                                {elseif $hel_rgpd_consentement_customer_detail["decision"] == 0}
                                                    <i class="icon-times-circle red"></i>
                                                {/if}
                                            </td>
                                        </tr>
                                    {if $smarty.foreach.myLoop.index % 10 == 9}
                                        </tbody>
                                    {/if}
                                    {/foreach}
                                    {*</div>*}
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            {/foreach}
        </div>
    {else}
        {l s="Vous n'avez pas encore de consentements en place sur ce site." mod='hel_rgpd'}
    {/if}
    {else}
    {l s="Veuillez vous connecter pour accéder à cette page." mod='hel_rgpd'}
{/if}

{*<script type="text/javascript">*}
{*$('.switch').on('click', function(){*}
{*console.log($(this).parent('span').parent('div').find('i'));*}
{*$(this).children('div').toggleClass('checked');*}
{*$(this).parent('span').parent('div').find('i').toggleClass('icon-check-circle');*}
{*$(this).parent('span').parent('div').find('i').toggleClass('icon-times-circle');*}
{*})*}

{*</script>*}