<?php

require_once(dirname(__FILE__) . '/../../hel_form.php');
require_once(dirname(__FILE__) . '/../../classes/HelFormClass.php');
require_once(dirname(__FILE__) . '/../../classes/HelFormSubmissionsClass.php');

class AdminHelFormController extends ModuleAdminController {

    public function __construct() {

        $this->bootstrap = true;
        $this->table = 'hel_form';
        $this->name = 'hel_form';
        $this->className = 'HelFormClass';
        $this->explicitSelect = false;
        $this->show_toolbar = false;
        $this->lang = false;
        $this->addRowAction = true;
        $this->context = Context::getContext();

        $this->fields_list = array(
            'id_hel_form' => array(
                'title' => 'ID',
                'align' => 'center',
                'class' => 'fixed-width-xs',
                'search' => false,
            ),
            'nameForm' => array(
                'title' => 'Titre',
                'search' => false,
            ),
            'urlForm' => array(
                'title' => 'URL',
                'search' => false,
            ),
            'tplForm' =>array(
                'title' => 'Fichier TPL',
                'search' => false,
            ),
            'processForm' =>array(
                'title' => 'Process du formulaire',
                'search' => false,
            ),
            'tableForm' =>array(
                'title' => 'Table correspondante',
                'search' => false,
            ),
            'sendEmailUserForm' => array(
                'title' => "Email envoyé utilisateur",
                'search' => false,
                'type'=>'bool',
                'active'=>'sendmail',
                'ajax' => true,
            ),
            'emailAdressAdminForm' =>array(
                'title' => 'Email responsable',
                'search' => false,
            ),
            'active' => array(
                'title' => 'Formulaire affiché',
                'search' => false,
                'type'=>'bool',
                'active'=>'activeform',
                'ajax' => true,
            ),
             'activeCaptcha' => array(
                'title' => 'Captcha activé',
                'search' => false,
                'type'=>'bool',
                'active'=>'activecaptcha',
                'ajax' => true,
            )
        );

        parent :: __construct();

    }

    public function renderList()
    {
        $this->addRowAction('view');
        $this->addRowAction('edit');
        return parent::renderList();
    }

    public function renderView()
    {
        Tools::redirectAdmin($this->context->link->getAdminLink('AdminHelFormSubmissions').'&id_hel_form='.(int)Tools::getValue('id_hel_form'));
        exit();
    }

    public function renderForm() {

        if (!$obj = $this->loadObject(true)) {
            return;
        }

        if (Validate::isLoadedObject($obj)) {
            $this->display = 'edit';
        } else {
            $this->display = 'add';
        }

        $this->fields_form = array(
            'legend' => array(
                'title' => 'Gestion d\'un formulaire'
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => 'Nom',
                    'name' => 'nameForm',
                    'required' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => 'URL',
                    'name' => 'urlForm',
                    'required' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => 'Fichier TPL',
                    'name' => 'tplForm',
                    'required' => true,
                    'hint' => "Par défaut : default.tpl"
                ),
                array(
                    'type' => 'text',
                    'label' => 'Process du formulaire',
                    'name' => 'processForm',
                    'required' => true,
                    'hint' => "Par défaut : processDefaultForm"
                ),
                array(
                    'type' => 'file',
                    'name' => 'dbFile',
                    'label' => 'Fichier de création de la table',
                    'required' => true,
                    'hint' => "Par défaut : module_folder/default_files/defaultSql.php"
                ),
                array(
                    'type' => 'text',
                    'label' => 'Table correspondante',
                    'name' => 'tableForm',
                    'required' => true,
                    'hint' => "Par défaut : hel_default_form"
                ),
                array(
                    'type' => 'switch',
                    'label' => 'Email envoyé utilisateur',
                    'name' => 'sendEmailUserForm',
                    'required' => true,
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'enableOn',
                            'value' => 1,
                            'label' => 'Oui'
                        ),
                        array(
                            'id' => 'enableOff',
                            'value' => 0,
                            'label' => 'Non'
                        )
                    )
                ),
                array(
                    'type' => 'text',
                    'label' => 'Email administrateur du formulaire',
                    'name' => 'emailAdressAdminForm',
                    'desc' => 'Plusieurs adresses séparées par des ; possible',
                    'required' => true,
                ),
                array(
                    'type' => 'textarea',
                    'label' => 'Message landing page',
                    'name' => 'messageLandingPage',
                    'required' => true,
                    'autoload_rte' => true,
                ),
                array(
                    'type' => 'switch',
                    'label' => 'Formulaire affiché',
                    'name' => 'active',
                    'required' => true,
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'enableOn',
                            'value' => 1,
                            'label' => 'Oui'
                        ),
                        array(
                            'id' => 'enableOff',
                            'value' => 0,
                            'label' => 'Non'
                        )
                    )
                ),
                array(
                    'type' => 'switch',
                    'label' => 'Captcha activé',
                    'name' => 'activeCaptcha',
                    'required' => true,
                    'is_bool' => true,
                    'desc' => 'Les clé publique et secrète doivent être renseignées dans la configuration du module',
                    'values' => array(
                        array(
                            'id' => 'enableOn',
                            'value' => 1,
                            'label' => 'Oui'
                        ),
                        array(
                            'id' => 'enableOff',
                            'value' => 0,
                            'label' => 'Non'
                        )
                    )
                ),
                array(
                    'type' => 'textarea',
                    'label' => 'JSON des champs',
                    'name' => 'jsonFieldsForm',
                    'disabled' => true,
                    'hint' => 'A remplir une fois le formulaire créé',
                ),
            )
        );

        $this->fields_form['submit'] = array(
            'title' => 'Enregistrer',
        );

        return parent::renderForm();
    }

    public function postProcess() {
        $db = Db::getInstance();
        if (Tools::isSubmit('submitAddhel_form')) {

            $message = Tools::getValue("messageLandingPage");

            $tplFormValue = Tools::getValue("tplForm");
            $dbFile =  $_FILES['dbFile'];

            if (preg_match("/^.*(.tpl)$/",$tplFormValue)!=1) {
                $this->errors[] = Tools::displayError("Format du fichier tpl invalide");
            }
            if(!file_exists(dirname(__FILE__) . '/../../views/templates/front/'.$tplFormValue)){
                $this->errors[] = Tools::displayError("Fichier TPL inexistant");
            }
            if (!method_exists('HelFormClass', Tools::getValue("processForm")))
            {
                $this->errors[] = Tools::displayError("Fonction process inexistante");
            }

            foreach (HelFormClass::getEmailsAdmin(Tools::getValue("emailAdressAdminForm")) as $email){
                if (!Validate::isEmail($email)){
                    $this->errors[] = Tools::displayError("Email incorrect : ".$email);
                }
            }

            $newTable = $this->importTableDB($dbFile);

            $sql = "SHOW TABLES LIKE '"._DB_PREFIX_.Tools::getValue("tableForm")."'";
            $res = $db->executeS($sql);
            if ($db->numRows($res) <1){
                $this->errors[] = Tools::displayError("La table n'existe pas");
            }

            $parentPost = parent::postProcess();

            $sql = "UPDATE `"._DB_PREFIX_."hel_form`
                    SET messageLandingPage = '".$db->escape($message,true,true)."' 
                    WHERE id_hel_form = ".$parentPost->id;
            $res = $db->execute($sql);

            if($newTable && count($this->errors)==0) {
                $this->createJSONFields($db, $parentPost);
            }
        }else{
            parent::postProcess();
        }
    }

    private function createJSONFields($db,$parentPost){

        $sql = "SELECT COLUMN_NAME
            FROM INFORMATION_SCHEMA.COLUMNS
            WHERE TABLE_NAME = N'"._DB_PREFIX_.Tools::getValue("tableForm")."'";
        $res = $db->executeS($sql);

        $fields = array();
        foreach ($res as $column){
            if (strpos($column['COLUMN_NAME'],'id') !== false
                || strpos($column['COLUMN_NAME'],'date_soumission') !== false){
                array_push($fields,array('column'=>$column['COLUMN_NAME'], 'name'=>'', 'displayListBO'=>true));
            }else {
                array_push($fields,array('column' =>$column['COLUMN_NAME'],'name'=>'','displayListBO'=>false));
            }
        }
        $jsonFields = $this->formatJSON(json_encode($fields));

        $sql = "UPDATE `"._DB_PREFIX_."hel_form`
                    SET jsonFieldsForm = '".$db->escape($jsonFields,true,true)."' 
                    WHERE id_hel_form = ".$parentPost->id;
        $db->execute($sql);
    }

    private function formatJSON($json){
        $lastPos = 0;
        while (($lastPos = strpos($json, "},", $lastPos))!== false) {
            $json = substr_replace($json, "\n", $lastPos+2, 0);
            $lastPos = $lastPos + strlen("},");
        }
        $lastPos = 0;
        while (($lastPos = strpos($json, '","', $lastPos))!== false) {
            $json = substr_replace($json, "\t\t\t", $lastPos+2, 0);
            $lastPos = $lastPos + strlen('","');
        }
        $lastPos = 0;
        while (($lastPos = strpos($json, ':', $lastPos))!== false) {
            $json = substr_replace($json, "   ", $lastPos+1, 0);
            $json = substr_replace($json, "   ", $lastPos, 0);
            $lastPos = $lastPos + strlen(':')+3;
        }
        return $json;
    }

    private function importTableDB($dbFile)
    {
        $uniqid = uniqid();
        $filename = _PS_DOWNLOAD_DIR_.'/sql_form_'.$uniqid.'.php';
        $newTable = false;
        if (!isset($dbFile) && !is_uploaded_file($dbFile['tmp_name'])){
            $this->errors[] = Tools::displayError("Le fichier n'a pas été téléchargé");
        }else if (is_uploaded_file($dbFile['tmp_name'])) {
            if (!move_uploaded_file($dbFile['tmp_name'],$filename))
            {
                if (!file_exists($filename) && isset($dbFile)) {
                    $this->errors[] = Tools::displayError("Le fichier n'a pas été déplacé");
                }
            }else{
                include($filename);
                $newTable = true;
            }
        }

        if (file_exists($filename)){
            unlink($filename);
        }

        return $newTable;
    }

    public function ajaxProcessSendmailhelform()
    {
        $hel_form = new HelFormClass((int)Tools::getValue('id_hel_form'));
        if (Validate::isLoadedObject($hel_form)) {
            $hel_form->sendEmailUserForm = $hel_form->sendEmailUserForm == 1 ? 0 : 1;
            $hel_form->save() ?
                die(Tools::jsonEncode(array('success' => true, 'text' => 'Le status a été mis à jour correctement'))) :
                die(Tools::jsonEncode(array('success' => false, 'error' => true, 'text' => 'Erreur lors de la mise à jour du status')));
        }
    }

    public function ajaxProcessActiveformhelform()
    {
        $hel_form = new HelFormClass((int)Tools::getValue('id_hel_form'));
        if (Validate::isLoadedObject($hel_form)) {
            $hel_form->active = $hel_form->active == 1 ? 0 : 1;
            $hel_form->save() ?
                die(Tools::jsonEncode(array('success' => true, 'text' => 'Le status a été mis à jour correctement'))) :
                die(Tools::jsonEncode(array('success' => false, 'error' => true, 'text' => 'Erreur lors de la mise à jour du status')));
        }
    }

    public function ajaxProcessActivecaptchahelform()
    {
        $hel_form = new HelFormClass((int)Tools::getValue('id_hel_form'));
        if (Validate::isLoadedObject($hel_form)) {
            $hel_form->activeCaptcha = $hel_form->activeCaptcha == 1 ? 0 : 1;
            $hel_form->save() ?
                die(Tools::jsonEncode(array('success' => true, 'text' => 'Le status a été mis à jour correctement'))) :
                die(Tools::jsonEncode(array('success' => false, 'error' => true, 'text' => 'Erreur lors de la mise à jour du status')));
        }
    }




}