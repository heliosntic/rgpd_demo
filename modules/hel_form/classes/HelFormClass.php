<?php

/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class HelFormClass extends ObjectModel
{
    /** @var int id */
    public $id_hel_form;

    /** @var string name */
    public $nameForm;

    /** @var string url */
    public $urlForm;

    /** @var string tpl file */
    public $tplForm;

    /** @var string process function name */
    public $processForm;

    /** @var string table associated with the form */
    public $tableForm;

    /** @var string json associated to the fields */
    public $jsonFieldsForm;

    /** @var bool send an email to the user of the form */
    public $sendEmailUserForm;

    /** @var string email adress of the administrator of the form*/
    public $emailAdressAdminForm;

    /** @var string email adress of the administrator of the form*/
    public $messageLandingPage;

    /** @var bool captcha activé*/
    public $activeCaptcha;

    /** @var bool active */
    public $active;

    public static $definition = array(
        'table' => 'hel_form',
        'primary' => 'id_hel_form',
        'fields' => array(
            'nameForm'              => array('type' => self::TYPE_STRING,   'validate' => 'isGenericName',  'required' => true, 'size' => 255),
            'urlForm'               => array('type' => self::TYPE_STRING,   'validate' => 'isGenericName',  'required' => true, 'size' => 255),
            'tplForm'               => array('type' => self::TYPE_STRING,   'validate' => 'isGenericName',  'required' => true, 'size' => 255),
            'processForm'           => array('type' => self::TYPE_STRING,   'validate' => 'isGenericName',  'required' => true, 'size' => 255),
            'tableForm'             => array('type' => self::TYPE_STRING,   'validate' => 'isGenericName',  'required' => true, 'size' => 255),
            'sendEmailUserForm'     => array('type' => self::TYPE_BOOL,     'validate' => 'isBool',         'required' => true),
            'emailAdressAdminForm'  => array('type' => self::TYPE_STRING,   'validate' => 'isCleanHtml',    'required' => false,'size' => 255),
            'jsonFieldsForm'        => array('type' => self::TYPE_STRING,   'validate' => 'isCleanHtml',    'required' => false),
            'messageLandingPage'    => array('type' => self::TYPE_STRING,   'validate' => 'isCleanHtml',    'required' => true, 'size' => 1024),
            'activeCaptcha'         => array('type' => self::TYPE_BOOL,     'validate' => 'isBool',         'required' => true),
            'active'                => array('type' => self::TYPE_BOOL,     'validate' => 'isBool',         'required' => true),
        ),
    );

    public function add($autodate = true, $null_values = false)
    {
        return parent::add($autodate, true);
    }

    public function update($null_values = false)
    {
        return parent::update($null_values);
    }

    public function delete()
    {
        if (parent::delete())
            return true;
        return false;
    }

    /**
     * @param $context the context of the application when the form was submitted
     * @param $emails an array that contains the information to send the emails to the user and the admin(s)
     *          $emails[0] : information for the email to the user, should contains :
     *                  * subject : subject of the mail
     *                  * template : name of the email template
     *                  * templatePath : path where 'template' is situated
     *                  * template_var : an array that contains the variable to fill the template
     *                  * emailTo : email adresse of the user
     *          $emails[1} : information for the emai lto the admin, should contains :
     *                  * subject : subject of the mail
     *                  * template : name of the email template
     *                  * templatePath : path where 'template' is situated
     *                  * template_var : an array that contains the variable to fill the template
     * @throws PrestaShopDatabaseException
     */
    public function sendEmail($context, $emails){

        $db = Db::getInstance();
        $sql = 'SELECT `sendEmailUserForm`, `emailAdressAdminForm`
        FROM '._DB_PREFIX_.'hel_form
        WHERE id_hel_form = '.Tools::getValue('id');
        $res = $db->executeS($sql);

        $emailUser = $emails[0];

        if ($res[0]['sendEmailUserForm'] && !empty($emailUser['emailTo']) && Validate::isEmail($emailUser['emailTo'])){
            Mail::Send((int)$context->language->id,$emailUser['template'],$emailUser['subject'],$emailUser['template_var'],
                $emailUser['emailTo'],null,null,null,null,null,
                $emailUser['templatePath'],false,$context->shop->id);

        }
        if (!empty($res[0]['emailAdressAdminForm'])){
            $emailAdmin= $emails[1];
            $emailAdressesAdmin = HelFormClass::getEmailsAdmin($res[0]['emailAdressAdminForm']);
            foreach ($emailAdressesAdmin as $emailAdress) {
                Mail::Send((int)$context->language->id, $emailAdmin['template'], $emailAdmin['subject'], $emailAdmin['template_var'],
                    $emailAdress, null, null, null, null, null,
                    $emailAdmin['templatePath'], false, $context->shop->id);
            }
        }
    }

    public static function getEmailsAdmin ($emailsAdminString){
        $lastPos = 0;
        $emailsAdmin = array();
        while (($newPos = strpos($emailsAdminString, ";", $lastPos))!== false)
        {
            $emailsAdmin[] = substr($emailsAdminString, $lastPos, $newPos);
            $lastPos = $newPos + strlen(";");
        }
        $emailsAdmin[] = substr($emailsAdminString, $lastPos, strlen($emailsAdminString));
        return $emailsAdmin;
    }

    public function redirectLandingPage($context,$urlReturn){
        $idForm = Tools::getValue("id");
        Tools::redirect($context->link->getModuleLink("hel_form","landing",array("id"=>$idForm,"url"=>$urlReturn)));
    }

    public function validateCaptcha(){
        $google_request_url =
            'https://www.google.com/recaptcha/api/siteverify?secret='.
            Configuration::get('HEL_FORM_CAPTCHA_SECRET_KEY').
            '&response='.Tools::getValue('g-recaptcha-response').
            '&remoteip='.$_SERVER['REMOTE_ADDR'];

        $ch = curl_init($google_request_url);
        curl_setopt_array($ch, array(
            CURLOPT_TIMEOUT => 10,
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_HEADER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
        ));
        $google_response = curl_exec($ch);
        curl_close($ch);

        if (version_compare(_PS_VERSION_, '1.7.0.0', '<')) {
            $google_response = Tools::jsonDecode($google_response);
        } else {
            $google_response = json_decode($google_response);
        }
        if (empty($google_response->success) || $google_response->success !== true) {
            return false;
        }

        return true;
    }

    /**Debut des process de gestion des formulaires**/
    public function processDefaultForm($valuesForm,$context){
        $errors = array();
        if (Tools::isSubmit('activeCaptcha') && Tools::getValue('activeCaptcha')==1) {
            $captcha = self::validateCaptcha();
            if (!$captcha) {
                $errors["captcha"] = Tools::displayError("Captcha invalide");
                return $errors;
            }
        }

        $db = Db::getInstance();
        $raisonSocial = $db->escape($valuesForm['raisonSocial'], true, true);
        $nom = $db->escape($valuesForm['nom'], true, true);
        $prenom = $db->escape($valuesForm['prenom'], true, true);
        $email = $db->escape($valuesForm['email'], true, true);
        $ville = $db->escape($valuesForm['ville'], true, true);
        $message = $db->escape($valuesForm['message'], true, true);
        $date = date("Y-m-d H:i:s");

        $sql = "INSERT INTO `" . _DB_PREFIX_ . "hel_default_form` (`raisonSocial`, `nom`,`prenom`,
        `email`,`ville`,`message`,`date_soumission`)
        VALUES ('" . $raisonSocial . "','" . $nom . "','" . $prenom . "','" . $email . "','"
            . $ville . "','" . $message . "','" . $date . "')";
        $db->execute($sql);

        $mailType = Configuration::get('PS_MAIL_TYPE');
        if ($mailType == Mail::TYPE_TEXT)
            $message = $valuesForm['message'];
        else
            $message = nl2br($valuesForm['message']);

        /**Prepare params for sending email**/
        $emails = array();

        $paramsUser = array(
            '{email}' => empty($email) ? $context->customer->email : $email,
            '{message}' => $message,
        );
        $emailUser = array(
            'subject' => "Default Form",
            'template' => 'defaultForm',
            'templatePath' => _PS_MODULE_DIR_ . "/hel_form/mails/",
            'template_var' => $paramsUser,
            'emailTo' => empty($email) ? $context->customer->email : $email,
        );

        array_push($emails, $emailUser);

        $paramsAdmin = array(
            '{email}' => $email,
            '{nom}' => $nom,
            '{prenom}' => $prenom,
            '{raisonSocial}' => $raisonSocial,
            '{ville}' => $ville,
            '{message}' => $message,
        );
        $emailAdmin = array(
            'subject' => "Résumé de demande de default form",
            'template' => 'adminDefaultForm',
            'templatePath' => _PS_MODULE_DIR_ . "/hel_form/mails/",
            'template_var' => $paramsAdmin,
        );
        array_push($emails, $emailAdmin);

        self::sendEmail($context, $emails);

        self::redirectLandingPage($context, '');
    }
}