<?php

/**
 * Installation par défaut
 */
$db = Db::getInstance();
$sql = array();

$sql[] = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "hel_form` (
	`id_hel_form` int(11) NOT NULL AUTO_INCREMENT,
	`nameForm` varchar(255) NOT NULL,
	`urlForm` varchar(255) NOT NULL,
	`tplForm` varchar(255) NOT NULL,
	`processForm` varchar(255) NOT NULL,
	`tableForm` varchar(255) NOT NULL,
	`jsonFieldsForm` TEXT,
	`sendEmailUserForm` tinyint(1) DEFAULT '0',
	`emailAdressAdminForm` varchar(255),
	`messageLandingPage` varchar (1024) NOT NULL,
	`activeCaptcha` tinyint(1) DEFAULT '0',
	`active` tinyint(1) DEFAULT '1',
	PRIMARY KEY (`id_hel_form`)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";

foreach ($sql as $query) {
    if ($db->execute($query) == false) {
        return false;
    }
}