<?php

$db = Db::getInstance();
$sql = "CREATE TABLE IF NOT EXISTS "._DB_PREFIX_."hel_default_form (
    `id_default_form` int(11) NOT NULL AUTO_INCREMENT,
    `raisonSocial` varchar(1024) NOT NULL,
	`nom` varchar(1024) NOT NULL,
	`prenom` varchar(1024) NOT NULL,
	`email` varchar(1024) NOT NULL,
	`ville` varchar(1024) NOT NULL,
	`message` varchar(2048) NOT NULL,
    `date_soumission` datetime NOT NULL,
PRIMARY KEY (`id_default_form`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";

if ($db->execute($sql) == false)
    return false;
