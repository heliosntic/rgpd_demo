<?php

class WebserviceRequest extends WebserviceRequestCore
{
    public static function getResources()
    {
        $resources = parent::getResources();
        Hook::exec('WebServiceGetResources', array('resources' => &$resources));
        return $resources;
    }
}
