<!--hel_clientlogin-->
{*
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from CREATYM
* Use, copy, modification or distribution of this source file without written
* license agreement from CREATYM is strictly forbidden.
* In order to obtain a license, please contact us: info@creatym.fr
* ...........................................................................
* INFORMATION SUR LA LICENCE D'UTILISATION
*
* L'utilisation de ce fichier source est soumise a une licence commerciale
* concedee par la societe CREATYM
* Toute utilisation, reproduction, modification ou distribution du present
* fichier source sans contrat de licence ecrit de la part de CREATYM est
* expressement interdite.
* Pour obtenir une licence, veuillez contacter CREATYM a l'adresse: info@creatym.fr
* ...........................................................................
* @package    Client login
* @copyright  Copyright (c) 2015 CREATYM (http://modules.creatym.fr)
* @author     Benjamin L.
* @license    Commercial license
* Support by mail  :  info@creatym.fr
* Support on forum :  clientlogin
* Phone : +33.87230110
*}
<script>
	var clientlogin_no_results_txt = "{l s='No customers have found on your shop.' mod='clientlogin'}";

	$(document).ready( function () {
		$('#dropdown_clientlogin').click(function (event) {
			var target = $(event.target);
			if (target.is('a') || target.is('a i')) {
				return true;
			}
			return false;
		});
	});
</script>
<li id="clientlogin_notif" style="background:none" class="dropdown">
	<a href="javascript:void(0);" class="dropdown-toggle clientlogin_notif" data-toggle="dropdown">
		<i class="icon-users"></i>
	</a>
	<div class="dropdown-menu notifs_dropdown" id="dropdown_clientlogin">
		<section id="clientlogin_notif_wrapper" class="notifs_panel" style="width:420px">
			<div class="notifs_panel_header">
				<h3>{l s='Client login' mod='clientlogin'}</h3>
			</div>
			<div class="clientlogin_search">
				<div class="clientlogin_search_input">
					<input id="clientlogin_search" class="form-control" type="text" name="clientlogin_search" value="" placeholder="{l s='Search a customer' mod='clientlogin'}">
				</div>
				
				<div id="clientlogin_search_result"></div>
			</div>
			{if isset($quick_access) && $quick_access}
			<div class="clientlogin_quick_access">
				<h3>{l s='Quick access' mod='clientlogin'}</h3>
				
				<div id="clientlogin_quick_access_result" class="panel clearfix" style="margin-bottom:0px; box-shadow:none;">
					<div class="panel-heading">
						<i class="icon-user"></i>
							{$quick_access_customer->firstname|escape:'htmlall':'UTF-8'} {$quick_access_customer->lastname|escape:'htmlall':'UTF-8'} [ {$quick_access_customer->id|escape:'htmlall':'UTF-8'} ] -
						<i class="icon-envelope"></i>
						<a class="dropdown_clientlogin_link" target="_blank" href="{$adminlogin_link|escape:'htmlall':'UTF-8'}&action=clientLoginDirectLink&id_customer={$quick_access_customer->id|escape:'htmlall':'UTF-8'}">{$quick_access_customer->email|escape:'htmlall':'UTF-8'}</a>
						<div class="panel-heading-action" style="float:right;">
							<a class="dropdown_clientlogin_link" target="_blank" href="{$adminlogin_link|escape:'htmlall':'UTF-8'}&action=clientLoginDirectLink&id_customer={$quick_access_customer->id|escape:'htmlall':'UTF-8'}">
								<i class="icon-chevron-circle-right"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
			{/if}
			{if isset($history_list) && $history_list}			
			<div class="clientlogin_history_list">
				<h3>{l s='Last search' mod='clientlogin'}</h3>
				
				<div id="clientlogin_history_result" class="panel clearfix" style="margin-bottom:0px; box-shadow:none;">
					{foreach from=$history_list item=customer name=customer_list}
						<div class="panel-heading {if $smarty.foreach.customer_list.iteration > 1}clientlogin_line{/if}">
							<i class="icon-user"></i>
							{$customer.firstname|escape:'htmlall':'UTF-8'} {$customer.lastname|escape:'htmlall':'UTF-8'} [ {$customer.id_customer|escape:'htmlall':'UTF-8'} ] - 
							<i class="icon-envelope"></i>
							<a class="dropdown_clientlogin_link" target="_blank" href="{$adminlogin_link|escape:'htmlall':'UTF-8'}&action=clientLoginDirectLink&id_customer={$customer.id_customer|escape:'htmlall':'UTF-8'}">{$customer.email|escape:'htmlall':'UTF-8'}</a>
							<div class="panel-heading-action" style="float:right;">
								<a class="dropdown_clientlogin_link" target="_blank" href="{$adminlogin_link|escape:'htmlall':'UTF-8'}&action=clientLoginDirectLink&id_customer={$customer.id_customer|escape:'htmlall':'UTF-8'}">
									<i class="icon-chevron-circle-right"></i>
								</a>
							</div>
						</div>
					{/foreach}
				</div>
			</div>
			{/if}
			<div class="notifs_panel_footer">
				<a href="{$link->getAdminLink('AdminClientLogin')|escape:'htmlall':'UTF-8'}">{l s='View historical' mod='clientlogin'}</a>
			</div>
		</section>
	</div>
</li>
<!--/hel_clientlogin-->