<?php

require_once(dirname(__FILE__) . '/../../classes/HelFormClass.php');

class hel_formdisplayModuleFrontController extends ModuleFrontController
{
    public $display_column_left = false;

    public function __construct()
    {
        parent::__construct();
        $this->context = Context::getContext();
    }

    public function initContent()
    {
        parent::initContent();
        $idForm = Tools::getValue('id');
        if (empty($idForm)) {
            Tools::redirect("pagenotfound");
        } else {
            $sql = 'SELECT `tplForm` , `active`, `activeCaptcha`
             FROM `' . _DB_PREFIX_ . 'hel_form`
             WHERE `id_hel_form` = ' . $idForm;
            $res = Db::getInstance()->executeS($sql);

            if (empty($res)) {
                Tools::redirect("pagenotfound");
            } else {
                if ($res[0]['active'] == 0) {
                    Tools::redirect("pagenotfound");
                } else {
                    $tplForm = $res[0]['tplForm'];
                    $this->context->smarty->assign('activeCaptcha',$res[0]['activeCaptcha']);
                    $this->context->smarty->assign('publicKey', Configuration::get('HEL_FORM_CAPTCHA_PUBLIC_KEY'));
                    $this->context->smarty->assign('css_dir', _THEME_CSS_DIR_);
                    if (isset($_SERVER["HTTP_REFERER"]) && !isset($this->context->smarty->tpl_vars['previous_page'])) {
                        $this->context->smarty->assign('previous_page', $_SERVER["HTTP_REFERER"]);
                    }
                    if (Module::isInstalled("hel_b2b") && Module::isEnabled("hel_b2b")) {
                        $current_b2b_user = new HelB2bUser($this->context->cookie->id_hel_b2b_users);
                        if (isset($this->context->customer->email)) {
                            $this->context->smarty->assign('email_user', $current_b2b_user->email);
                        }
                    }else{
                        if (isset($this->context->customer->email)) {
                            $this->context->smarty->assign('email_user', $this->context->customer->email);
                        }
                    }

                    if (Tools::isSubmit('reference')) {
                        $this->context->smarty->assign('refProduit', Tools::getValue('reference'));
                    }
                    $this->setTemplate($tplForm);
                }
            }
        }

    }

    public function postProcess()
    {
        parent::postProcess();
        if (Tools::isSubmit('previousPageURL') && Tools::getValue('previousPageURL')!=''){
            $this->context->smarty->assign('previous_page',Tools::getValue('previousPageURL'));
        }
        if (Tools::isSubmit('submitForm') && Tools::isSubmit('id')) {
            $idForm = Tools::getValue('id');
            $sql = 'SELECT `tplForm` , `active`, `processForm`
             FROM `' . _DB_PREFIX_ . 'hel_form`
             WHERE `id_hel_form` = ' . $idForm;
            $res = Db::getInstance()->executeS($sql);

            $processName = $res[0]['processForm'];

            if (method_exists('HelFormClass', $processName)) {
                $hel_form = new HelFormClass();
                $errors = call_user_func(array($hel_form, $processName), $_POST,$this->context);
                $this->context->smarty->assign('errorsCustom', $errors);
                foreach ($_POST as $key=>$value){
                    $this->context->smarty->assign($key, $value);
                }
                return;
            } else {
                Tools::redirect("pagenotfound");
            }

        }
    }
}