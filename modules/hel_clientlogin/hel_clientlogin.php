<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from CREATYM
 * Use, copy, modification or distribution of this source file without written
 * license agreement from CREATYM is strictly forbidden.
 * In order to obtain a license, please contact us: info@creatym.fr
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe CREATYM
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de CREATYM est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter CREATYM a l'adresse: info@creatym.fr
 * ...........................................................................
 *
 * @package   Client login
 * @author    Benjamin L.
 * @copyright 2015 Créatym <http://modules.creatym.fr>
 * @license   Commercial license
 * Support by mail  :  info@creatym.fr
 * Support on forum :  clientlogin
 * Phone : +33.87230110
 */
require_once(dirname(__FILE__) . '/classes/ClientLoginHistory.php');

if (!defined('_PS_VERSION_'))
	exit;

class Hel_clientLogin extends Module {

	private $html = '';
	private $post_errors = array();
	private $filters = array();
	private $base_url;
	private $config = array(
	);

	public function __construct() {
		$this->name = 'hel_clientlogin';

		$this->tab = 'front_office_features';

		$this->version = '1.1.0';
		$this->ps_versions_compliancy['min'] = '1.5';
		$this->author = 'Helios';
		$this->bootstrap = true;
		$this->module_key = 'c6421a87111b902716fa0b35cfb38b23';

		parent::__construct();

		$this->secure_key = Tools::encrypt($this->name);

		/* The parent construct is required for translations */

		$this->page = basename(__FILE__, '.php');
		$this->displayName = $this->l('Client login');
		$this->description = $this->l('Connect to your customer account without asking their customer IDs.');
		$this->confirmUninstall = $this->l('Are you sure you want to uninstall this module ?');

		$this->img_path = $this->_path . 'views/img/';
		$this->css_path = $this->_path . 'views/css/';
		$this->js_path = $this->_path . 'views/js/';

		/* Module link */
		$this->module_link = _PS_MODULE_DIR_ . $this->name . '/';
	}

	public function install() {
		if (!parent::install() || !$this->installTab() || !$this->executeSQLFile('install.sql') || !$this->registerHook('displayAdminCustomers') || !$this->registerHook('displayAdminOrder') || !$this->registerHook('displayTop') || !$this->registerHook('displayBackOfficeHeader') || !$this->registerHook('displayFooter') || !$this->registerHook('displayHeader') || !$this->registerHook('actionValidateOrder') || !$this->registerHook('displayShoppingCart') || !$this->registerHook('actionCustomerLogoutAfter'))
			return false;

		foreach ($this->config as $key => $value)
			if (!Configuration::updateValue($key, $value))
				return false;

		return true;
	}

	public function uninstall() {
		if (!parent::uninstall() || !$this->uninstallTab() || !$this->executeSQLFile('uninstall.sql') || !$this->unregisterHook('displayAdminCustomers') || !$this->unregisterHook('displayAdminOrder') || !$this->unregisterHook('displayTop') || !$this->unregisterHook('displayBackOfficeHeader') || !$this->unregisterHook('displayFooter') || !$this->unregisterHook('displayHeader'))
			return false;

		foreach (array_keys($this->config) as $key)
			if (!Configuration::deleteByName($key))
				return false;

		return true;
	}

	public function executeSQLFile($file) {
		$path = realpath(_PS_MODULE_DIR_ . $this->name) . DIRECTORY_SEPARATOR . 'sql' . DIRECTORY_SEPARATOR; // @since 1.3.3.0

		if (!file_exists($path . $file))
			$path = realpath(_PS_MODULE_DIR_ . $this->name) . DIRECTORY_SEPARATOR;

		if (!file_exists($path . $file)) {
			$this->post_errors[] = 'File not found : ' . $path . $file;
			$this->_errors[] = 'File not found : ' . $path . $file;
			return false;
		}

		if (!($sql = Tools::file_get_contents($path . $file))) {
			$this->post_errors[] = 'File empty : ' . $path . $file;
			$this->_errors[] = 'File empty : ' . $path . $file;
			return false;
		}

		$sql = preg_split("/;\s*[\r\n]+/", str_replace('PREFIX_', _DB_PREFIX_, $sql));
		$db = Db::getInstance();

		foreach ($sql as $query) {
			$query = trim($query);

			if ($query) {
				if (!$db->Execute($query)) {
					$this->post_errors[] = $db->getMsgError() . ' ' . $query;
					$this->_errors[] = $db->getMsgError() . ' ' . $query;
					return false;
				}
			}
		}
		return true;
	}

	public function installTab() {
		$tab = new Tab();
		$tab->active = 1;
		$tab->class_name = 'AdminClientLogin';
		$tab->name = array();
		$tab->name[Configuration::get('PS_LANG_DEFAULT')] = 'Client login';
		$tab->name[Language::getIdByIso('en')] = 'Client login';
		$tab->name[Language::getIdByIso('fr')] = 'Client login';
		$tab->name[Language::getIdByIso('es')] = 'Client login';
		$tab->name[Language::getIdByIso('de')] = 'Client login';
		$tab->name[Language::getIdByIso('it')] = 'Client login';
		$tab->id_parent = -1;
		$tab->module = $this->name;
		return $tab->add();
	}

	public function uninstallTab() {
		$id_tab = (int) Tab::getIdFromClassName('AdminClientLogin');
		if ($id_tab) {
			$tab = new Tab($id_tab);
			return $tab->delete();
		} else
			return false;
	}

	public function hookDisplayBackOfficeHeader() {

		if (!in_array($this->context->controller, array("AdminOrdersController", "AdminCustomersController"))) {
			return false;
		}
		//check if currently updatingcheck if module is currently processing update
		if (!Module::isEnabled($this->name))
			return false;

		if (method_exists($this->context->controller, 'addJquery')) {
			$this->context->controller->addJquery();

			if (version_compare(_PS_VERSION_, '1.6.0', '>=') === true) {
				$this->context->controller->addCss($this->_path . 'views/css/admin_bt.css');
				$this->context->controller->addJs($this->_path . 'views/js/clientlogin_bt.js');
			} else {
				$this->context->controller->addCss($this->_path . 'views/css/admin.css');
				$this->context->controller->addJs($this->_path . 'views/js/clientlogin.js');
			}

			$this->context->controller->addJqueryPlugin('fancybox');
			Media::addJsDef(array(
				'admin_clientlogin_ajax_url' => $this->context->link->getAdminLink('AdminClientLogin'),
				'admincustomer_token' => Tools::getAdminTokenLite('AdminCustomers'),
				'client_login_customer' => (Tools::getIsset(Tools::getValue('id_customer')) ? Tools::getValue('id_customer') : ''),
				'client_login_order' => (Tools::getIsset(Tools::getValue('id_order')) ? Tools::getValue('id_order') : '')
			));
//			return '<script>
//				var admin_clientlogin_ajax_url = \''.$this->context->link->getAdminLink('AdminClientLogin').'\';
//				var admincustomer_token = \''.Tools::getAdminTokenLite('AdminCustomers').'\';
//				var client_login_customer = \''.(Tools::getIsset(Tools::getValue('id_customer')) ? Tools::getValue('id_customer'):'').'\';
//				var client_login_order = \''.(Tools::getIsset(Tools::getValue('id_order')) ? Tools::getValue('id_order'):'').'\';
//			</script>';
		}
	}

	public function renderHeaderNotification() {
		if ($id_customer = Tools::getValue('client_login_customer')) {
			$customer = new Customer((int) $id_customer);

			$this->context->smarty->assign(array(
				'quick_access' => true,
				'quick_access_customer' => $customer,
			));
		}

		if ($id_order = Tools::getValue('client_login_order')) {
			$order = new Order((int) $id_order);
			$customer = new Customer((int) $order->id_customer);

			$this->context->smarty->assign(array(
				'quick_access' => true,
				'quick_access_customer' => $customer,
			));
		}

		$client_history = ClientLoginHistory::getHistory(3);

		if ($client_history) {
			$this->context->smarty->assign(array(
				'history_list' => $client_history,
			));
		}

		$this->context->smarty->assign(array(
			'link' => $this->context->link,
			'adminlogin_link' => $this->context->link->getAdminLink('AdminClientLogin')
		));

		if (version_compare(_PS_VERSION_, '1.6.0', '>='))
			return $this->display(__FILE__, 'notification_bt.tpl');
		else
			return $this->display(__FILE__, 'notification.tpl');
	}

	private function postProceed() {
		$cookie_lifetime = (int) defined('_PS_ADMIN_DIR_') ? Configuration::get('PS_COOKIE_LIFETIME_BO') : Configuration::get('PS_COOKIE_LIFETIME_FO');
		$cookie_lifetime = time() + (max($cookie_lifetime, 1) * 3600);

		if (Context::getContext()->shop->getGroup()->share_order)
			$cookie = new Cookie('ps-sg' . Context::getContext()->shop->getGroup()->id, '', $cookie_lifetime, Context::getContext()->shop->getUrlsSharedCart());
		else {
			$domains = null;

			if (Context::getContext()->shop->domain != Context::getContext()->shop->domain_ssl)
				$domains = array(Context::getContext()->shop->domain_ssl, Context::getContext()->shop->domain);

			$cookie = new Cookie('ps-s' . Context::getContext()->shop->id, '', $cookie_lifetime, $domains);
		}

		if (Tools::isSubmit('submitClientLogin')) {
			if ($cookie->logged)
				$cookie->logout();

			Tools::setCookieLanguage();
			Tools::switchLanguage();

			$customer = new Customer((int) Tools::getValue('id_customer'));
			$cookie->id_customer = (int) $customer->id;
			$cookie->customer_lastname = $customer->lastname;
			$cookie->customer_firstname = $customer->firstname;
			$cookie->logged = 1;
			$cookie->passwd = $customer->passwd;
			$cookie->email = $customer->email;

			$context = Context::getContext();
			$employee = new Employee($context->employee->id);
			$cookie->id_employee = $context->employee->id;
			$cookie->employee_lastname = $employee->lastname;
			$cookie->employee_firstname = $employee->firstname;

            if (Module::isInstalled("hel_b2b") && Module::isEnabled("hel_b2b") &&  Configuration::get('HEL_B2B_MULTI_USERS', false)) {
                $cookie->id_hel_b2b_users = HelB2bUser::getFirstByCustomer($customer->id);
            }



			if (Configuration::get('PS_CART_FOLLOWING') && (empty($cookie->id_cart) || Cart::getNbProducts($cookie->id_cart) == 0))
				$cookie->id_cart = Cart::lastNoneOrderedCart($customer->id);

			if (Tools::getValue('used_last_cart'))
				$cookie->id_cart = $this->getLastCart($customer->id);

			$clientLoginHistory = new ClientLoginHistory();
			$clientLoginHistory->id_customer = (int) $customer->id;
			$clientLoginHistory->id_employee = (int) Context::getContext()->employee->id;
			$clientLoginHistory->date_add = date('Y-m-d H:i:s');
			$clientLoginHistory->add();
		}

		return $cookie;
	}

	public static function getLastCart($id_customer, $with_order = true) {
		$carts = Cart::getCustomerCarts((int) $id_customer, $with_order);
		if (!count($carts))
			return false;
		$cart = array_shift($carts);
		$cart = new Cart((int) $cart['id_cart']);
		return ($cart->nbProducts() === 0 ? false : (int) $cart->id);
	}

	public function hookDisplayAdminCustomers($params) {
		$cookie = $this->postProceed();

		if (isset($params['id_customer'])) {
			$id_customer = (int) $params['id_customer'];
			$customer = new Customer($id_customer);
			$page_name = 'customer';
		} else {
			$order = new Order($params['id_order']);
			$id_customer = (int) $order->id_customer;
			$customer = new Customer($id_customer);
			$page_name = 'order';
		}

		if ($cookie->logged && $cookie->id_customer == $id_customer)
			$this->smarty->assign('customer_logged', true);
		else
			$this->smarty->assign('customer_logged', false);

		$this->smarty->assign(array(
			'customer' => $customer,
			'page_name' => $page_name,
		));

		return $this->display(__FILE__, 'views/templates/admin/admin.tpl');
	}

	public function hookDisplayAdminOrder($params) {
		return $this->hookDisplayAdminCustomers($params);
	}

	protected function checkEnvironment() {
		$cookie = new Cookie('psAdmin', '', (int) Configuration::get('PS_COOKIE_LIFETIME_BO'));
		return isset($cookie->id_employee) && isset($cookie->passwd) && Employee::checkPassword($cookie->id_employee, $cookie->passwd);
	}

	public function getLiveClientLoginToken() {
		return Tools::getAdminToken($this->name . (int) Tab::getIdFromClassName($this->name)
						. (is_object(Context::getContext()->employee) ? (int) Context::getContext()->employee->id :
								Tools::getValue('id_employee')));
	}

	public function hookActionCustomerLogoutAfter($params) {
		$context = Context::getContext();

		$context->cookie->id_employee = "";
		$context->cookie->employee_lastname = "";
		$context->cookie->employee_firstname = "";
	}

	public function hookActionValidateOrder($params) {
		//ALTER TABLE  `bar_orders` ADD  `id_employee` INT NULL
		$context = Context::getContext();
//		krumo($params);
//		krumo($context->cookie->id_employee);
		if (isset($context->cookie->id_employee) && $context->cookie->id_employee != "") {
			$order = $params['order'];
			$order->id_employee = $context->cookie->id_employee;
			$order->save();
		}
	}

	public function hookDisplayShoppingCart($params) {
		$context = Context::getContext();
		if ($context->cookie->id_employee) {
			return $this->display(__FILE__, 'hookDisplayShoppingCart.tpl');
		}
	}

}

?>